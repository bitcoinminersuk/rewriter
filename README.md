#rewriter

##Dependencies

This software requires some external libraries to work, these are seperated into two different catogories - system packages and python packages - system packages must be installed before python packages will work correctly...

###Pillow

Find more information here for pillow:

http://pillow.readthedocs.io/en/4.2.x/installation.html

###PyAutoGUI

PyAutoGUI requires some external software to work also, please find more information here:

http://pyautogui.readthedocs.io/en/latest/install.html

##Installing Chrome
Install the latest version of Google Chrome.

###Chromedriver

Download a version of chromedriver that works with your version of Chrome, from the following link:

https://chromedriver.storage.googleapis.com/index.html

Once downloaded copy or move the file as superuser, a good way to do this is to open the terminal (CTRL + ALT + T) and type the following to open a file browser with elevated permissions: 

```sh
$ sudo su
# nautilus
```
This should open a file browser that has access to root(system) level files and folders, be very careful in this mode!

Locate the downloaded file and cut and paste it into: 

```
/usr/local/share
```
As this is a system protected directory you can only do this with elevated privileges so please use with caution, always close the file browser immediately afterwards so as to keep your system safe from accidentally deleting important files.

###Python 3

Rewriter requires Python 3 be installed, it can be downloaded and installed by following the instructions at this link:

https://www.python.org/downloads/

###Creating a virtual environment

####Installation
It is important to create a python virtual environment before installing any python packages.

To check if you already have virtualenv installed type:

```
$ virtualenv --version
```
If you get a version number back then you already have it and you can skip the next step and create a virtualenv to make a segregated python environment, if not you can install it using:
```
$ sudo apt-get install python-virtualenv
```
####Creation
It is advisable to create your virtual environment alongside - on the same folder level - as the rewriter folder.
To create a virtual environment in the folder you already reside in, type:

```
$ virtualenv <YourVirtualEnvName>
```
####Activating your virtual environment
To activate your virtual environment, type:
```
$ source <YourVirtualEnvName>/bin/activate
```
####Deactivating your virtual environment
To deactivate your virtual environment, simply type:
```
$ deactivate
```

###Installing Python requirements
Make sure your virtual environment is activated and you are inside the rewriter directory and type:
```
$ pip install -r requirements.txt
```
This will install all the python packages that will make rewriter2.py work.