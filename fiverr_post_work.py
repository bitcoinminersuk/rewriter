import os
from random import randrange
import time
from selenium import webdriver
import pyautogui
import dropbox

import os

# ---------------------------- #
# | commonly shared settings | #
# ---------------------------- #

BASE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)))
DOWNLOADS_DIRECTORY = os.path.join(os.sep, "home", "chris", "Downloads")
SPINBOT_API_KEY = "69698b98c90c43cbb5497deee642294f"
GRAMMARLY_USERNAME = "chris99hansen@gmail.com"
GRAMMARLY_PASSWORD = "christ9999"

# ------------------- #
# | fiverr settings | #
# ------------------- #

FIVERR_USERNAME = "unison1978@gmail.com"
FIVERR_PASSWORD = "Kyriacou50"
# fiverr_service app
FIVERR_DROPBOX_ACCESS_TOKEN = 'WBbA7tcNZ5AAAAAAAAAALLa17fOrs-Sze_mat1WFav6I3q4ojPkHW83dyjlVrpid'

# --------------------------------- #
# | dummy settings template header| #
# --------------------------------- #

# check folder for changes
# put orders in a dictionary with a random time between 2 and 5 hours
# read that dictionary every minute and compare time with current time
# if times match or are greater than, then login to fiverr and send message with link

dbx = dropbox.Dropbox(FIVERR_DROPBOX_ACCESS_TOKEN)


def fiverr_login(browser, username, password):
    # try to find the dashboard icon to check if we are already logged in
    try:
        browser.find_element_by_css_selector(
            "#main-wrapper-header>header>div>div>nav.main-nav.main-nav-user>ul>li.menu-icn.icn-todo.hint--bottom.js-mark>a")
        already_logged_in = True
    except:  # NoSuchElementException:
        # if exception is rased then it means that the icon was not found which means we are logged out
        already_logged_in = False
    if not already_logged_in:
        browser.get("https://www.fiverr.com/")
        time.sleep(4)
        # click sign in link
        browser.find_element_by_css_selector("#main-wrapper-header>header>div.mp-box.mp-box-opaque>div>nav.main-nav.main-nav-guest>ul>li:nth-child(2)>a").click()
        # click login with google button
        browser.find_element_by_css_selector("#js-popup-user>div>div.popup-content-login>div.social-login-buttons.cf>a").click()
        time.sleep(4)
        # switch to the google window
        browser.switch_to_window("Fiverr_Google")
        # enter the username
        browser.find_element_by_css_selector("#identifierId").send_keys(username)
        # click the next button
        browser.find_element_by_css_selector("#identifierNext>content>span").click()
        time.sleep(3)
        # enter the password
        browser.find_element_by_css_selector("#password>div.aCsJod.oJeWuf>div>div.Xb9hP>input").send_keys(password)
        # click the next button
        browser.find_element_by_css_selector("#passwordNext>content>span").click()
        # switch back to the main window
        browser.switch_to_window("")
        time.sleep(3)
        # refresh the page
        browser.refresh()
        # we are now logged in phew!
        time.sleep(5)


def upload_files_to_dropbox(order_number):
    """
    Takes in an order number and gets all files that are contained in that order number folder,
    sends them to dropbox in and puts them in a folder named "Order - <order_number>"
    :param order_number:
    Takes a String that is used to read and create folders and files of the same name,
    if the folder and/or files with the corresponding order number embeded into them do not exist
    then this wont work.
    :return:
    Returns a list of dictionaries for each file it uploaded / or not as the case may be!
    """
    order_folder_for_dropbox = "Order - " + order_number
    files = os.listdir(os.path.join(os.getcwd(), "orders", order_number))  # get all files in directory
    response = []
    for file in files:
        with open(os.path.join(os.getcwd(), "orders", order_number, file), "rb") as f:
            try:
                response = dbx.files_upload(f.read(), os.path.join(os.sep, order_folder_for_dropbox, file))
                time.sleep(20)
            except:
                print("Tried to upload the files but they were already there! skipping...")
    # move the files on our end into a completed folder
    os.rename(os.path.join(os.getcwd(), "orders", order_number),
              os.path.join(os.getcwd(), "completed", order_number))
    return response


def get_dropbox_share_link(order_number):
    return dbx.sharing_create_shared_link_with_settings(os.path.join(os.sep, "Order - " + order_number)).url


def add_random_times():
    folders = os.listdir(os.path.join(os.getcwd(), "orders"))  # get all folders
    # for every key in the dictionary give it a random time
    order_delay_times = {}
    for folder in folders:
        print("folder " + folder)
        folder_date = os.stat(os.path.join(os.getcwd(), "orders", folder)).st_mtime
        print("folder date " + str(folder_date))
        if not order_delay_times.get(folder):
            order_delay_times[folder] = folder_date + randrange((60 * 2)*60, (60 * 5)*60)
            print("order delay time are .. " + str(order_delay_times))
    return order_delay_times


def send_order(order_dict, browser):
    for item in order_dict:
        print("item " + item)
        time_now = time.time()
        order_time = order_dict[item]
        if time_now > order_time:
            print(str(time.time()) + " time now")
            print(str(order_dict[item]) + " time order is supposed to happen")
            fiverr_login(browser, FIVERR_USERNAME, FIVERR_PASSWORD)
            try:
                try:
                    browser.find_element_by_xpath('//*[@id="action-bar"]/div/div/div[3]/p')
                    os.rename(os.path.join(os.getcwd(), "orders", item),
                              os.path.join(os.getcwd(), "completed", item))
                except:
                    browser.get("https://www.fiverr.com/users/christophers150/manage_orders/" + item)
                    # get the "deliver now" button
                    time.sleep(4)
                    browser.find_element_by_xpath('//*[@id="action-bar"]/div/div/div[1]/a').click()
                    # wait for the form to appear
                    time.sleep(6)
                    # upload the files
                    upload_files_to_dropbox(item)
                    time.sleep(30)

                    # get a link for the customer to access their files
                    share_link = get_dropbox_share_link(item)
                    thank_you_message = """As promised, I have supplied you with a revised version of your article within the 24-hour window.
        
                            Included is a Grammarly report to certify the material has been optimised to be pass error and plagiarism thresholds.
        
                            Here is the link to your revised files: {}
        
                            I would appreciate a good review and hope to do further business with you.
        
                            Kindest regards
        
        
                            Chris""".format(share_link)
                    browser.find_element_by_xpath('//*[@id="msg_body"]').send_keys(thank_you_message)
                    browser.find_element_by_xpath('//*[@id="new_delivery_v2"]/footer/div/input').click()
                    time.sleep(10)
                    # the fucking wankers have poped up a box asking if we really want to complete the order without uploading files
                    location = pyautogui.locateCenterOnScreen(os.path.join(BASE_DIR, "pics", "gay_ok_button.png"))
                    pyautogui.click(location)
                    time.sleep(5)
            except:  # NoSuchElementException:
                pass


def main(browser):
    time.sleep(10)
    while True:
        random_order_times = add_random_times()
        send_order(random_order_times, browser)
        for order in random_order_times:
            print("Order number: " + order + " has: " + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(random_order_times[order])))


if __name__ == "__main__":
    chrome = webdriver.Chrome()
    main(chrome)
