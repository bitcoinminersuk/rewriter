import os
import time
from datetime import datetime
from slugify import slugify
from selenium import webdriver


def blog_login(browser, username, password):
    browser.get("https://bitcoinminersuk.com/accounts/login/")
    time.sleep(5)
    input_boxes = browser.find_elements_by_tag_name("input")
    input_boxes[2].send_keys(username)
    input_boxes[3].send_keys(password)
    input_boxes[4].click()  # the submit button


def blog_submission(browser, final_text="", title="", image_filename=""):
    blog_login(browser, "bitcoinminersuk", "N3crophil3")
    time.sleep(4)
    browser.get("https://bitcoinminersuk.com/blog/create/")
    time.sleep(3)
    blog_title = browser.find_element_by_css_selector("#id_title")
    blog_title.click()
    blog_title.send_keys(title)
    time.sleep(2)
    blog_body = browser.find_element_by_css_selector("#id_content")
    blog_body.click()
    blog_body.send_keys(final_text)
    time.sleep(10)
    blog_image = browser.find_element_by_id("id_image")
    print(blog_image.get_attribute("outerHTML"))
    # cmd = "document.getElementById('id_image').sendKeys = '" + open(os.path.abspath(image_filename)) + "';"
    # browser.execute_script(cmd)
    # image_filepath = os.path.join(os.path.abspath(image_filename))
    # print(image_filepath)
    # spun_file = image_filepath.split("/")[-1]
    # print(spun_file)
    blog_image.send_keys(os.path.abspath(image_filename))
    # blog_image.send_keys()
    # blog_image.click()
    # window = pyautogui.getWindow("File Upload")
    # blog_image.send_keys(os.path.join(os.getcwd(), slugify(spun_title) + "." + extension))
    # /home/chris/Desktop/projects/rewriter/rewriter/why-bcash-mining-shouldn-t-influence-bitcoin-much-however-bitcoin-mining-could-destroy-bcash.jpeg
    time_now = datetime.now()
    day_element = browser.find_element_by_css_selector("#id_publish_day")
    day_element.click()
    day_element.send_keys(time_now.day)
    time.sleep(3)
    month_element = browser.find_element_by_css_selector("#id_publish_month")
    month_element.click()
    month_element.send_keys(time_now.month)
    time.sleep(3)
    year_element = browser.find_element_by_css_selector("#id_publish_year")
    year_element.click()
    year_element.send_keys(time_now.year)
    time.sleep(3)
    #/home/james/Desktop/article-rewriter/rewriter-python-3/south-korean-market-giant-bans-bitcoin-mining-amidst-electricity-outages.jpg
    #browser.find_element_by_id("id_image").send_keys(os.getcwd()+"/image.png")
    time.sleep(10)
    blog_submit = browser.find_element_by_css_selector("body > div:nth-child(4) > form > input.btn.btn-default")
    blog_submit.click()


def test_blog(browser, title="", final_text="", image_filename=""):
    blog_submission(browser, title=title, final_text=final_text, image_filename=image_filename)


firefox = webdriver.Chrome()
test_blog(firefox,
          title="why-bcash-mining-shouldn-t-influence-bitcoin-much-however-bitcoin-mining-could-destroy-bcash",
          final_text="some dummy text",
          image_filename="img.jpeg")
