from selenium import webdriver
import time
import os
from pprint import pprint
from selenium.common.exceptions import (NoSuchElementException,
                                        ElementNotVisibleException,
                                        ElementNotInteractableException,
                                        StaleElementReferenceException,)
from slugify import slugify
import dropbox
import requests
import pyautogui

# ---------------------------- #
# | commonly shared settings | #
# ---------------------------- #

BASE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)))
DOWNLOADS_DIRECTORY = os.path.join(os.sep, "home", "chris", "Downloads")
SPINBOT_API_KEY = "69698b98c90c43cbb5497deee642294f"
GRAMMARLY_USERNAME = "chris99hansen@gmail.com"
GRAMMARLY_PASSWORD = "christ9999"

# ------------------- #
# | fiverr settings | #
# ------------------- #

FIVERR_USERNAME = "unison1978@gmail.com"
FIVERR_PASSWORD = "Kyriacou50"
# fiverr_service app
FIVERR_DROPBOX_ACCESS_TOKEN = 'WBbA7tcNZ5AAAAAAAAAALLa17fOrs-Sze_mat1WFav6I3q4ojPkHW83dyjlVrpid'

# --------------------------------- #
# | dummy settings template header| #
# --------------------------------- #

"""This module serves to automate a article rewrite service on fiverr.com"""

dbx = dropbox.Dropbox(FIVERR_DROPBOX_ACCESS_TOKEN)


def spinbot_it(title="", text="", ignore_words=""):
    print("")
    print("-------------------")
    print("| spinbot section |")
    print("-------------------")
    print("")
    # initial values so if something goes wrong it returns something useful
    spun_article = {"title": "Spinbot service is down", "text": "Spinbot service is down"}
    # dummy = {
    #     "x-auth-key": SPINBOT_API_KEY,
    #     "x-multi-creds": "true",
    #     "x-spin-cap-words": "[true | false]",
    #     "x-words-to-skip": ignore_words
    #     "x-min-percent-change-per-sentence": "[int between 1 - 100]",  # default is "any"
    #     "x-action": "getavailablespins"
    # }
    request_headers = {
        "x-auth-key": SPINBOT_API_KEY,
        "x-words-to-skip": ignore_words
    }

    request_available_spins = {
        "x-auth-key": SPINBOT_API_KEY,
        "x-action": "getavailablespins"
    }

    payload = (title + "\n" + text).encode(encoding='utf-8')
    # text version that comes in at the top
    article = title + "\n" + text
    print("")
    print("--------------------------")
    print("| Text before conversion |")
    print("--------------------------")
    print("")
    pprint(article)
    print("")
    # The next line gets the available spin credits only
    r = requests.post('https://api.spinbot.com', data="", headers=request_available_spins)
    print(r)
    if r.status_code == 200:
        print(str(r.status_code) + " " + r.reason)
        print("you see it work!")
        print("Available Spins Left: " + str(r.headers["available-spins"]))
        if int(r.headers["available-spins"]) > 1:
            # the next line spins it
            r = requests.post('https://api.spinbot.com', data=payload, headers=request_headers)
            print("spinning...")
            print("Available Spins Left: " + str(r.headers["available-spins"]))
            time.sleep(10)
            article = r.content.decode('utf-8')
            print("")
            print("-------------------------")
            print("| Text After conversion |")
            print("-------------------------")
            print("")
            pprint(article)
            parts = list(article.split("\n"))
            title = parts.pop(0)
            spun_article = {"title": title, "text": article}
        else:
            print("You have no available credits go to: https://spinbot.com/Manage to top up")
            spun_article = {"title": "no spinbot credit", "text": "no spinbot credit"}
    else:
        print(str(r.status_code) + " " + r.reason)
        print("you see it not work!")
    return spun_article


def grammarly_login(browser, username, password):
    time.sleep(1)
    browser.get("https://www.grammarly.com/signin?page=free")
    input_boxes = browser.find_elements_by_tag_name("input")
    input_boxes[0].send_keys(username)
    input_boxes[1].send_keys(password)
    # button = browser.find_element_by_xpath('//*[@id="page"]/div/div/div/form/div[1]/div[3]/button/span')
    button = browser.find_element_by_tag_name('button')
    button.click()  # the submit button
    time.sleep(6)


def grammarly_new_file_click(browser):
    time.sleep(2)
    new_grammarly_file = browser.find_element_by_css_selector("._4fdedb-add")
    new_grammarly_file.click()
    time.sleep(2)


def load_text_from_file(filename=""):
    with open(filename, "r") as f:
        text = f.read()
    return text


def switch_on_plagiarism(browser):
    time.sleep(1)
    element1 = browser.find_element_by_xpath('//*[@id="page"]/div/div/div/div[2]/div[1]/div[5]/div[2]')
    browser.execute_script("arguments[0].setAttribute('class', '_747451-item _747451-genreItem _747451-hover')", element1)
    time.sleep(1)
    browser.find_element_by_xpath('//*[@id="page"]/div/div/div/div[2]/div[1]/div[5]/div[2]/div[2]/div/div[1]').click()
    time.sleep(4)
    browser.execute_script("arguments[0].setAttribute('class', '_747451-item _747451-genreItem')", element1)


def grammarly_paste_text(browser, spun_article=None):
    time.sleep(1)
    text_area = browser.find_element_by_tag_name("textarea")
    text_area.click()
    if spun_article is None:
        spun_article = load_text_from_file("what-s-next-for-bitcoin-money-making-profitless-mining-beneficial.txt")
        # text_area.send_keys(spun_article)
        browser.execute_script("arguments[0].value = arguments[1]", text_area, spun_article)
    else:
        # text_area.send_keys(spun_article)
        browser.execute_script("arguments[0].value = arguments[1]", text_area, spun_article)
    return text_area


def renew_cards(browser):
    cards = browser.find_elements_by_class_name("cardBaseClassName")
    return cards


def make_report(browser, order_number=""):
    # create report here
    # get the score
    time.sleep(10)
    element = browser.find_element_by_class_name('_ff9902-value')
    score = element.text
    if int(score) >= 95:
        time.sleep(2)
        # click score button
        element.click()
        time.sleep(2)
        plag_score = browser.find_elements_by_class_name('_e118f2-row')[2].find_element_by_class_name('_e118f2-value')
        if int(str(plag_score.text).strip('%')) <= 5:
            time.sleep(2)
            # click download button
            if os.path.isfile(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf")):
                # delete it first
                os.remove(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf"))
                time.sleep(2)
                # then download it
                browser.find_element_by_xpath(
                    '//*[@id="page"]/div/div/div/div[3]/div/div/div/div[2]/div[3]/div[1]').click()
                # after downloaded move it to the customers folder
                time.sleep(20)
                os.makedirs(os.path.join(os.getcwd(), "orders", order_number), exist_ok=True)
                os.rename(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf"),
                          os.path.join(os.getcwd(), "orders", order_number, order_number + "-report.pdf"))
            else:
                # download it
                browser.find_element_by_xpath(
                    '//*[@id="page"]/div/div/div/div[3]/div/div/div/div[2]/div[3]/div[1]').click()
                # after downloaded move it to the customers folder
                time.sleep(20)
                os.makedirs(os.path.join(os.getcwd(), "orders", order_number), exist_ok=True)
                os.rename(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf"),
                          os.path.join(os.getcwd(), "orders", order_number, order_number + "-report.pdf"))


def grammarly_it(browser, spun_article=None, filename="", extension="", order_number=""):
    grammarly_login(browser, GRAMMARLY_USERNAME, GRAMMARLY_PASSWORD)
    grammarly_new_file_click(browser)
    text_area = grammarly_paste_text(browser, spun_article)
    switch_on_plagiarism(browser)
    # record time here
    timeout = time.time() + 60 * 10  # set for 10 minutes at the moment
    while True:
        text_area.click()
        cards = renew_cards(browser)
        print(len(cards))
        try:
            for i, card in enumerate(cards):
                if card.text != "Undo":
                    # information part
                    print("Card number: " + str(i))
                    print("Total number of cards: " + str(len(cards)))
                    print("--------------------")
                    print("Card classes yaarrr!")
                    print("--------------------")
                    try:
                        print(card.get_attribute("class"))
                    except NoSuchElementException:
                        print("no more cards! yay!")
                    print("-------------------------------------------")
                    print("Card text, its not rocket science, darling!")
                    print("-------------------------------------------")
                    print(card.text)
                    try:
                        spans = card.find_elements_by_tag_name("span")
                    except NoSuchElementException:
                        pass
                    print("------------------------")
                    print("Card Span classes, yarr!")
                    print("------------------------")
                    for span in spans:
                        print(span.get_attribute("class"))
                    print("---------------------------------------------------------------------------------------")
                    # working part
                    location = card.location_once_scrolled_into_view
                    chrome.execute_script(
                        "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                    if card.text == "Squinting modifier" or "Wordiness" or "Dangling modifier" or "Incomplete comparison" or "Passive voice" or "Incorrect verb":
                        try:
                            ignore_button = card.find_element_by_class_name("_ed4374-btnIgnore")
                            try:
                                ignore_button.click()
                            except ElementNotVisibleException:
                                print("not visible")
                                pass
                            break
                        except:
                            pass
                        continue
                    if card.text == "Possible Americanism" or "Unusual word pair":
                        card.click()
                        # time.sleep(1)
                        try:
                            span_element = card.find_element_by_class_name("_929504-listItemReplacement")
                            location = span_element.location_once_scrolled_into_view
                            print(location)
                            chrome.execute_script(
                                "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                            # time.sleep(1)
                            try:
                                span_element.click()
                            except ElementNotVisibleException:
                                print("not visible")
                                pass
                            break
                        except:
                            pass
                    if len(str(card.text).split()) > 1:
                        if str(card.text).split()[0] + " " + str(card.text).split()[1] == "Overused word:" or "Weak adjective:" or "Repetitive word:":
                            card.click()
                            # time.sleep(1)
                            try:
                                span_element = card.find_element_by_class_name("_929504-listItemReplacement")
                                location = span_element.location_once_scrolled_into_view
                                print(location)
                                chrome.execute_script(
                                    "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                # time.sleep(1)
                                try:
                                    span_element.click()
                                except ElementNotVisibleException:
                                    print("not visible")
                                    pass
                                break
                            except:
                                pass
                    if len(str(card.text).split()) > 1:
                        if str(card.text).split()[0] + " " + str(card.text).split()[1] == "Unoriginal text:":
                            try:
                                span_element = card.find_element_by_class_name("_ed4374-btnIgnore")
                                try:
                                    span_element.click()
                                except ElementNotVisibleException:
                                    print("not visible")
                                    pass
                                break
                            except:
                                pass
                    if card.text == "Possibly confused word":
                        card.click()
                        # time.sleep(1)
                        list_item_replacement = card.find_element_by_class_name("_929504-listItemReplacement")
                        location = list_item_replacement.location_once_scrolled_into_view
                        chrome.execute_script(
                            "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                        # time.sleep(1)
                        list_item_replacement.click()
                    for span in spans:
                        if span.get_attribute("class") == "_929504-insertPart":
                            try:
                                print("deletePart")
                                location = span.location_once_scrolled_into_view
                                print(location)
                                chrome.execute_script(
                                    "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                # time.sleep(1)
                                try:
                                    span.click()
                                except ElementNotVisibleException:
                                    pass
                                break
                            except ElementNotInteractableException:
                                pass
                        elif span.get_attribute("class") == "_929504-insertReplacement":
                            try:
                                print("insertReplacement")
                                # span.location_once_scrolled_into_view
                                location = span.location_once_scrolled_into_view
                                print(location)
                                chrome.execute_script(
                                    "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                # time.sleep(1)
                                try:
                                    span.click()
                                except ElementNotVisibleException:
                                    pass
                                break
                            except ElementNotInteractableException:
                                pass
                        elif span.get_attribute("class") == "_929504-deletePunctuation":
                            try:
                                print("deletePunctuation")
                                # span.location_once_scrolled_into_view
                                location = span.location_once_scrolled_into_view
                                print(location)
                                chrome.execute_script(
                                    "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                # time.sleep(1)
                                try:
                                    span.click()
                                except ElementNotVisibleException:
                                    pass
                                break
                            except ElementNotInteractableException:
                                pass
                        elif span.get_attribute("class") == "_929504-deleteReplacement":
                            try:
                                print("deleteReplacement")
                                # span.location_once_scrolled_into_view
                                location = span.location_once_scrolled_into_view
                                print(location)
                                chrome.execute_script(
                                    "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                # time.sleep(1)
                                try:
                                    span.click()
                                except ElementNotVisibleException:
                                    pass
                                break
                            except ElementNotInteractableException:
                                pass
                        elif span.get_attribute("class") == "_929504-deletePart":
                            try:
                                print("insertPart")
                                # span.location_once_scrolled_into_view
                                location = span.location_once_scrolled_into_view
                                print(location)
                                chrome.execute_script(
                                    "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                # time.sleep(1)
                                try:
                                    span.click()
                                except ElementNotVisibleException:
                                    pass
                                break
                            except ElementNotInteractableException:
                                pass
                        elif card.text == "Incorrect quantifier":
                            try:
                                location = card.location_once_scrolled_into_view
                                print(location)
                                chrome.execute_script(
                                    "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                # time.sleep(1)
                                try:
                                    ignore_button = card.find_element_by_class_name("_ed4374-btnIgnore")
                                    ignore_button.click()
                                except ElementNotVisibleException:
                                    pass
                            except ElementNotInteractableException:
                                pass
                    print("Card number: " + str(i))
                    print("Total number of cards: " + str(len(cards)))
                    print("---------")
                    print("Card text")
                    print("---------")
                    print(card.text)
                    print("-------------------------------------")
                    print("Time left till timeout occurs: " + str(time.time() - timeout))
                    print("-------------------------------------")
            if len(cards) <= 1:
                print("there is nothing left to process")
                break
            if time.time() > timeout:
                # time has reached 0 so give the results so far
                # there was a cyclical loop so inform admin by sending an email
                # TODO send email about grammarly timeout due to cyclical loop
                text_area.click()
                all_text = text_area.get_attribute("value")
                make_report(browser, order_number=order_number)
                with open(os.path.join("orders", order_number, slugify(filename.strip(".txt")) + '-' + order_number + "-corrected." + extension), "w") as f:
                    f.write(all_text)
                return {"final_text": all_text}
        except StaleElementReferenceException:
            pass
    # this part happens if eveything went well
    text_area.click()
    all_text = text_area.get_attribute("value")
    make_report(browser, order_number=order_number)
    # write the corrected version to disk
    with open(os.path.join("orders", order_number, slugify(filename.strip(".txt")) + '-' + order_number + "-corrected." + extension), "w") as f:
        f.write(all_text)
    # remove the doc that we just created so that there is no trace we have been there!
    remove_the_latest_grammarly_document_created(browser)
    return {"final_text": all_text}


def remove_the_latest_grammarly_document_created(browser):
    # assumes we are logged in
    trash_element = browser.find_element_by_xpath(
        '//*[@id="page"]/div/div/div/div[2]/div[4]/div/div[3]/div[2]/div[3]/div/div/div')
    if browser.current_url == 'https://app.grammarly.com/':
        trash_element.click()
    else:
        browser.get('https://app.grammarly.com/')
        trash_element.click()


def fiverr_login(browser, username, password):
    browser.get("https://www.fiverr.com/")
    # click sign in link
    browser.find_element_by_css_selector("#main-wrapper-header>header>div.mp-box.mp-box-opaque>div>nav.main-nav.main-nav-guest>ul>li:nth-child(2)>a").click()
    # click login with google button
    browser.find_element_by_css_selector("#js-popup-user>div>div.popup-content-login>div.social-login-buttons.cf>a").click()
    time.sleep(4)
    # switch to the google window
    browser.switch_to_window("Fiverr_Google")
    # enter the username
    browser.find_element_by_css_selector("#identifierId").send_keys(username)
    # click the next button
    browser.find_element_by_css_selector("#identifierNext>content>span").click()
    time.sleep(3)
    # enter the password
    browser.find_element_by_css_selector("#password>div.aCsJod.oJeWuf>div>div.Xb9hP>input").send_keys(password)
    # click the next button
    browser.find_element_by_css_selector("#passwordNext>content>span").click()
    # switch back to the main window
    browser.switch_to_window("")
    time.sleep(3)
    # refresh the page
    browser.refresh()
    # we are now logged in phew!
    time.sleep(5)


def go_to_order_page(browser):
    browser.get("https://www.fiverr.com/")
    # click on the dashboard icon
    browser.find_element_by_css_selector(
        "#main-wrapper-header>header>div>div>nav.main-nav.main-nav-user>ul>li.menu-icn.icn-todo.hint--bottom.js-mark>a").click()
    time.sleep(2)
    # click on the "sales in que" number
    browser.find_element_by_css_selector(
        "#main-wrapper>div.main-content>section>div.box-row.p-b-100>div>nav>a:nth-child(1)").click()
    time.sleep(2)


def click_on_top_gig(browser):
    # click on the gig link
    try:
        browser.refresh()
        time.sleep(2)
        browser.find_element_by_xpath('//*[@id="manage_sales"]/div/div[2]/table/tbody/tr/td[4]/div[1]/div/a').click()
        time.sleep(2)
    except NoSuchElementException:
        print("there are no gigs!")
        # wait 10 minutes
        for i in range(60, -1, -1):
            time.sleep(1)
            print("time in seconds left till next retry: " + str(i))
        click_on_top_gig(browser)


def get_gig_requirements(browser):
    # get the order number
    order_number = browser.find_element_by_xpath('//*[@id="main-wrapper"]/div[4]/div/section/div[1]/article/header/h1').text
    order_number = str(order_number).split()[1].strip('#')
    # get the file url
    try:
        file_link = browser.find_element_by_xpath(
            '//*[@id="main-wrapper"]/div[4]/div/section/div[1]/article/section[2]/article[1]/div/ol/li[2]/aside/ul/li/a[2]')
    except:
        pass
    # file_link = browser.find_element_by_xpath(
    #     '//*[@id="main-wrapper"]/div[4]/div/section/div[1]/article/section[3]/article[1]/div/ol/li[2]/aside/ul/li/a[2]')

    message_section = browser.find_element_by_xpath('//*[@id="main-wrapper"]/div[4]/div/section/div[1]/article/section[3]')
    last_article_message = message_section.find_elements_by_tag_name('article')[-1]
    first_aside = last_article_message.find_element_by_tag_name('aside')
    file_link = first_aside.find_elements_by_tag_name('a')[-2]
    filename = str(first_aside.find_element_by_tag_name('li').text).split('\n')[0]
    print(filename)
    time.sleep(2)
    # extract the url
    url = file_link.get_attribute("href")
    print(url)
    # download the file using the url, it puts it into the Downloads directory
    browser.get(url=url)
    time.sleep(10)
    # get the files name

    # try:
    #     filename = browser.find_element_by_xpath(
    #         '//*[@id="main-wrapper"]/div[4]/div/section/div[1]/article/section[2]/article[1]/div/ol/li[2]/aside/ul/li/a[2]/span[1]').text
    # except:
    #     filename = browser.find_element_by_xpath(
    #         '//*[@id="main-wrapper"]/div[4]/div/section/div[1]/article/section[3]/article[1]/div/ol/li[2]/aside/ul/li/a[2]/span[1]').text
    # pprint(filename)
    # get file path including filename in the downloads directory
    filepath = os.path.join(os.sep, "home", "chris", "Downloads", filename)
    print(filepath)
    # load the file from the disk and store the file contents in a variable
    content = load_text_from_file(filename=filepath)
    # get the negative keywords from fiverr
    negative_keywords = browser.find_element_by_xpath(
        '//*[@id="main-wrapper"]/div[4]/div/section/div[1]/article/section[3]/article[1]/div/ol/li[3]/div/p[2]').text
    # niche word
    niche_word = browser.find_element_by_xpath(
        '//*[@id="main-wrapper"]/div[4]/div/section/div[1]/article/section[3]/article[1]/div/ol/li[4]/div/p[2]').text
    return filename, content, negative_keywords, niche_word, order_number


def niche_lab(browser, niche_word):
    browser.get('http://www.nichelaboratory.com/')
    browser.find_element_by_xpath('//*[@id="txtKeywords"]').send_keys(niche_word)
    browser.find_element_by_xpath('//*[@id="btnGetKeywords"]').click()
    time.sleep(20)
    browser.find_element_by_xpath('/html/body/div[3]/div/div[1]/div/div[2]/div/div[2]/ul/li[4]/a').click()
    extra_keywords = browser.find_element_by_xpath('/html/body/div[3]/div/div[1]/div/div[2]/div/div[2]/div/div[4]/div/div/div[2]/p').text
    return extra_keywords


def upload_files_to_dropbox(order_number):
    """
    Takes in an order number and gets all files that are contained in that order number folder,
    sends them to dropbox in and puts them in a folder named "Order - <order_number>"
    :param order_number:
    Takes a String that is used to read and create folders and files of the same name,
    if the folder and/or files with the corresponding order number embeded into them do not exist
    then this wont work.
    :return:
    Returns a list of dictionaries for each file it uploaded / or not as the case may be!
    """
    order_folder_for_dropbox = "Order - " + order_number
    files = os.listdir(os.path.join(os.getcwd(), "orders", order_number))  # get all files in directory
    response = []
    for file in files:
        with open(os.path.join(os.getcwd(), "orders", order_number, file), "rb") as f:
            try:
                response = dbx.files_upload(f.read(), os.path.join(os.sep, order_folder_for_dropbox, file))
                time.sleep(20)
            except:
                print("Tried to upload the files but they were already there! skipping...")
    return response


def get_dropbox_share_link(order_number):
    return dbx.sharing_create_shared_link_with_settings(os.path.join(os.sep, "Order - " + order_number)).url


def repeat_youtube_video_forever(browser, video_id):
    browser.get("https://www.youtube.com/v/" + video_id + "?rel=0&amp;loop=1&amp;controls=0&amp;showinfo=0&amp;autoplay=1&amp;playlist=LlhKZaQk860")


def process_order(browser):
    go_to_order_page(browser)
    click_on_top_gig(browser)
    filename, content, negative_keywords, niche_word, order_number = get_gig_requirements(browser)
    # extra_keywords = niche_lab(browser, niche_word)
    # negative_keywords += extra_keywords
    split_parts = content.split("\n")
    title = split_parts[0]
    body = split_parts[2]
    spun_article = spinbot_it(title=title, text=body, ignore_words=negative_keywords)

    if len(spun_article["text"]) < 10000:
        final_text = grammarly_it(browser, spun_article=spun_article["text"], filename=filename, extension="txt", order_number=order_number)
        print("")
        print("------------------------")
        print("| Text after Grammarly |")
        print("------------------------")
        print("")
        pprint(final_text["final_text"])
        # send file to client
        go_to_order_page(browser)
        click_on_top_gig(browser)
        # get the "deliver now" button
        browser.find_element_by_xpath('//*[@id="action-bar"]/div/div/div[1]/a').click()
        # wait for the form to appear
        time.sleep(10)

        # upload the files
        upload_files_to_dropbox(order_number)
        time.sleep(30)

        # get a link for the customer to access their files
        share_link = get_dropbox_share_link(order_number)
        thank_you_message = """As promised, I have supplied you with a revised version of your article within the 24-hour window.

        Included is a Grammarly report to certify the material has been optimised to be pass error and plagiarism thresholds.
        
        Here is the link to your revised files: {}

        I would appreciate a good review and hope to do further business with you.

        Kindest regards


        Chris""".format(share_link)
        browser.find_element_by_xpath('//*[@id="msg_body"]').send_keys(thank_you_message)
        browser.find_element_by_xpath('//*[@id="new_delivery_v2"]/footer/div/input').click()
        time.sleep(10)
        # the fucking wankers have poped up a box asking if we really want to complete the order without uploading files
        location = pyautogui.locateCenterOnScreen(os.path.join(BASE_DIR, "pics", "gay_ok_button.png"))
        pyautogui.click(location)
        time.sleep(5)


def main(browser):
    fiverr_login(browser, FIVERR_USERNAME, FIVERR_PASSWORD)
    process_order(browser)
    for i in range(10, -1, -1):
        time.sleep(1)
        print("time in seconds left till next retry: " + str(i))
    repeat_youtube_video_forever("LlhKZaQk860")


if __name__ == "__main__":
    chrome = webdriver.Chrome()
    main(chrome)
