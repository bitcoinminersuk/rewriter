import os
import time
import requests
import re
from slugify import slugify
from ast import literal_eval
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver import Chrome
from selenium.common.exceptions import (NoSuchElementException,
                                        ElementNotVisibleException,
                                        ElementNotInteractableException,
                                        StaleElementReferenceException,
                                        WebDriverException,)
import sys
from settings import (SPINBOT_API_KEY, GRAMMARLY_USERNAME, GRAMMARLY_PASSWORD, FIVERR_USERNAME, FIVERR_PASSWORD,
                      FIVERR_DROPBOX_ACCESS_TOKEN, DOWNLOADS_DIRECTORY, BASE_DIR, ORDERS_PATH, COMPLETED_PATH)


def replace_utf_with_ascii(content):
    if content:
        content = re.sub(u"\u2014", "--", content)
        content = re.sub(u"\u201c", '"', content)
        content = re.sub(u"\u201d", '"', content)
        content = re.sub(u"\u2019", "'", content)
    return content


def extract_text_and_title(content):
    if content:
        content = replace_utf_with_ascii(content)
        split_content = str(content).split("\n")
        print(split_content)
        title = split_content.pop(0)
        if split_content == "":
            split_content.pop(0)
        text = ''.join(split_content)
        return title, text


def spinbot_it(title="", text="", ignore_words=""):
    print("")
    print("-------------------")
    print("| spinbot section |")
    print("-------------------")
    print("")
    # initial values so if something goes wrong it returns something useful
    spun_article = {"title": "Spinbot service is down", "text": "Spinbot service is down"}
    # dummy = {
    #     "x-auth-key": SPINBOT_API_KEY,
    #     "x-multi-creds": "true",
    #     "x-spin-cap-words": "[true | false]",
    #     "x-words-to-skip": ignore_words
    #     "x-min-percent-change-per-sentence": "[int between 1 - 100]",  # default is "any"
    #     "x-action": "getavailablespins"
    # }
    # this part fixes issues with ignore_words making spinbot crash
    ignore_words = ignore_words.replace("\r\n", " ")
    ignore_words = ignore_words.replace("\n", " ")

    request_headers = {
        "x-auth-key": SPINBOT_API_KEY,
        "x-multi-creds": "true",
        "x-words-to-skip": ignore_words
    }

    request_available_spins = {
        "x-auth-key": SPINBOT_API_KEY,
        "x-action": "getavailablespins"
    }

    payload = (title + "\n" + text).encode(encoding='utf-8').decode()
    # The next line gets the available spin credits only
    r = requests.post('https://api.spinbot.com', data="", headers=request_available_spins)
    print(r)
    if r.status_code == 200:
        print(str(r.status_code) + " " + r.reason)
        print("you see it work!")
        print("Available Spins Left: " + str(r.headers["available-spins"]))
        if int(r.headers["available-spins"]) > 1:
            # the next line spins it
            r = requests.post('https://api.spinbot.com', data=payload, headers=request_headers)
            print("spinning...")
            print("Available Spins Left: " + str(r.headers["available-spins"]))
            print(r.content.decode('ascii'))
            print(r.text)
            parts = list(r.text.split("\n"))
            title = parts.pop(0)
            if parts[0] == "":
                parts.pop(0)
            article = "".join(parts)
            spun_article = {"title": title, "text": article}
        else:
            print("You have no available credits go to: https://spinbot.com/Manage to top up")
            spun_article = {"title": "no spinbot credit", "text": "no spinbot credit"}
    else:
        print(str(r.status_code) + " " + r.reason)
        print("you see it not work!")
    return spun_article


def grammarly_login(browser, username, password):
    browser.get("https://www.grammarly.com/signin?page=free")
    input_boxes = browser.find_elements_by_tag_name("input")
    input_boxes[0].send_keys(username)
    input_boxes[1].send_keys(password)
    browser.find_element_by_tag_name('button').click()
    time.sleep(6)


def grammarly_new_file_click(browser):
    time.sleep(2)
    browser.find_element_by_css_selector("._4fdedb-add").click()
    time.sleep(8)


def load_text_from_file(filename=""):
    with open(filename, "r") as f:
        text = f.read()
    return text


def grammarly_switch_on_plagiarism(browser):
    elem = browser.find_element_by_xpath('//*[@id="page"]/div/div/div/div[2]/div[1]/div[5]/div[2]')
    browser.execute_script("arguments[0].setAttribute('class', '_747451-item _747451-genreItem _747451-hover')", elem)
    time.sleep(1)
    browser.find_element_by_xpath('//*[@id="page"]/div/div/div/div[2]/div[1]/div[5]/div[2]/div[2]/div/div[1]').click()
    time.sleep(4)
    browser.execute_script("arguments[0].setAttribute('class', '_747451-item _747451-genreItem')", elem)


def grammarly_paste_text(browser, text=None):
    time.sleep(1)
    text_area = browser.find_element_by_tag_name("textarea")
    if text is None:
        text = load_text_from_file("what-s-next-for-bitcoin-money-making-profitless-mining-beneficial.txt")
        browser.execute_script("arguments[0].value = arguments[1]", text_area, text)
    else:
        browser.execute_script("arguments[0].value = arguments[1]", text_area, text)
    return text_area


def renew_cards(browser):
    cards = browser.find_elements_by_class_name("cardBaseClassName")
    return cards


def make_report(browser, order=""):
    time.sleep(10)  # wait here is only so we can see the results, it's not necessary
    element = browser.class_name('_ff9902-value')
    score = element.text
    if int(score) >= 95:
        time.sleep(2)
        # click score button
        element.click()
        time.sleep(2)
        plag_score = browser.class_names('_e118f2-value')[2]
        if int(str(plag_score.text).strip('%')) <= 5:
            time.sleep(2)
            xpath = '//*[@id="page"]/div/div/div/div[3]/div/div/div/div[2]/div[3]/div[1]'
            if os.path.isfile(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf")):  # delete it if it exists
                os.remove(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf"))
                time.sleep(2)
                browser.xpath_click_with_delay(xpath, 20)
                os.renames(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf"),
                           os.path.join(BASE_DIR, "orders", order, order + "-report.pdf"))
            else:
                browser.xpath_click_with_delay(xpath, 20)
                os.renames(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf"),
                           os.path.join(BASE_DIR, "orders", order, order + "-report.pdf"))


def scroll_to(browser, x=None, y=None):
    if x and y is not None:
        browser.execute_script(
            'window.scrollTo(' + str(x) + ', ' + str(y) + ');')
    elif x is not None:
        browser.execute_script(
            'window.scrollTo(' + str(x) + ', ' + '"0");')
    elif y is not None:
        browser.execute_script(
            'window.scrollTo("0", ' + str(y) + ');')
    else:
        # does nothing if no coordinates are passed into the top
        pass


def grammarly_click_inside_card(browser, card):
    card.find_element_by_class_name("_ed4374-header").click()
    time.sleep(1)
    span_element = card.find_element_by_class_name("_929504-listItemReplacement")
    location = span_element.location_once_scrolled_into_view
    scroll_to(browser, x=0, y=location["y"])
    time.sleep(1)
    span_element.click()
    print("clicked on " + span_element.text)


def remove_the_latest_grammarly_document_created(browser):
    if browser.current_url == 'https://app.grammarly.com/':
        trash_element = browser.find_element_by_xpath(
            '//*[@id="page"]/div/div/div/div[2]/div[4]/div/div[3]/div[2]/div[3]/div/div/div')
        trash_element.click()
    else:
        browser.goto_site_with_delay('https://app.grammarly.com/', 4)
        trash_element = browser.find_element_by_xpath(
            '//*[@id="page"]/div/div/div/div[2]/div[4]/div/div[3]/div[2]/div[3]/div/div/div')
        trash_element.click()


def grammarly_it(browser, spun_article=None, filename="", extension="", order_number=""):
    # login
    grammarly_login(browser, GRAMMARLY_USERNAME, GRAMMARLY_PASSWORD)
    # click new
    grammarly_new_file_click(browser)
    # paste text
    text_area = grammarly_paste_text(browser, spun_article)
    # switch on plagiarism
    grammarly_switch_on_plagiarism(browser)
    # click on stuff lol
    ignore_cards = ["Squinting modifier", "Wordiness", "Dangling modifier", "Incomplete comparison", "Passive voice", "Incorrect verb", "Incorrect quantifier"]
    click_in_card = ["Possible Americanism", "Unusual word pair"]
    click_in_card_ending_in_colon = ["Overused word", "Weak adjective", "Repetitive word", "Possibly miswritten word"]
    timeout = time.time() + 60 * 10  # set for 10 minutes at the moment
    while True:
        text_area.click()
        cards = browser.find_elements_by_class_name('cardBaseClassName')
        print("**** renewed cards *****")
        for card in cards:
            try:
                if str(card.text) != "Undo":
                    print("")
                    print(card.text)
                    location = card.location_once_scrolled_into_view
                    scroll_to(browser, y=location["y"])
                    spans = card.find_elements_by_tag_name('span')
                    for span in spans:
                        if span.get_attribute("class") == "_929504-insertPart":
                            print("insertPart")
                            location = span.location_once_scrolled_into_view
                            scroll_to(browser, y=location["y"])
                            time.sleep(1)
                            span.click()
                        elif span.get_attribute("class") == "_929504-insertReplacement":
                            print("insertReplacement")
                            location = span.location_once_scrolled_into_view
                            scroll_to(browser, y=location["y"])
                            time.sleep(1)
                            span.click()
                        elif span.get_attribute("class") == "_929504-deletePunctuation":
                            print("deletePunctuation")
                            location = span.location_once_scrolled_into_view
                            scroll_to(browser, y=location["y"])
                            time.sleep(1)
                            span.click()
                        elif span.get_attribute("class") == "_929504-deleteReplacement":
                            print("deleteReplacement")
                            location = span.location_once_scrolled_into_view
                            scroll_to(browser, y=location["y"])
                            time.sleep(1)
                            span.click()
                        elif span.get_attribute("class") == "_929504-deletePart":
                            print("deletePart")
                            location = span.location_once_scrolled_into_view
                            scroll_to(browser, y=location["y"])
                            time.sleep(1)
                            span.click()
                    if str(card.text) in click_in_card:
                        grammarly_click_inside_card(browser, card)
                    elif str(card.text).split(':')[0] in click_in_card_ending_in_colon:
                        grammarly_click_inside_card(browser, card)
                    elif str(card.text) == "Possibly confused word":
                        grammarly_click_inside_card(browser, card)
                    elif str(card.text) in ignore_cards:
                        card.find_element_by_class_name("_ed4374-btnIgnore").click()
                        print("clicked ignore on " + card.text)
                    elif str(card.text).split(':')[0] == "Unoriginal text":
                        card.find_element_by_class_name("_ed4374-btnIgnore").click()
                        print("clicked ignore on " + card.text)
                    # card.find_element_by_class_name("_ed4374-header").click()
                    time.sleep(2)
            except StaleElementReferenceException:
                print("stale")
                break
            except NoSuchElementException:
                pass
            except WebDriverException:
                pass
        text_area.click()
        time.sleep(10)
        if len(cards) <= 1:
            print("there is nothing left to process")
            break
        if time.time() > timeout:
            # time has reached 0 so give the results so far
            # there was a cyclical loop so inform admin by sending an email
            # TODO send email about grammarly timeout due to cyclical loop
            text_area.click()
            all_text = text_area.get_attribute("value")
            make_report(order=order_number)
            with open(os.path.join("orders", order_number, slugify(
                    filename.strip(".txt")) + '-' + order_number + "-corrected." + extension), "w") as f:
                f.write(all_text)
            return {"final_text": all_text}
    all_text = text_area.get_attribute("value")
    make_report(order=order_number)
    # write the corrected version to disk
    with open(os.path.join("orders", order_number,
                           slugify(filename.strip(".txt")) + '-' + order_number + "-corrected." + extension),
              "w") as f:
        f.write(all_text)
    # remove the doc that we just created so that there is no trace we have been there!
    remove_the_latest_grammarly_document_created(browser)
    return {"final_text": all_text}


def download_original_file():
    # Check if file is in order_status
    # If file is not in, download to orders
    # If file is in orders proceed to the next stage

    pass





if __name__ == "__main__":
    sys.path.append(BASE_DIR)
    text = load_text_from_file('/home/chris/Desktop/projects/rewriter/rewriter/google-says-russia-s-enthusiasm-for-bitcoin-up-220-mining-560-ethereum-760.txt')
    browser = Chrome()
    grammarly_it(browser, spun_article=text)
