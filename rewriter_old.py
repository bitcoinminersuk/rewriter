from io import BytesIO
from newspaper import Article
import os
import pyautogui
import requests
import time
from PIL import Image
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from slugify import slugify

# !!!!!!!!!!!!!!!!!!!!  TO DO LIST !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# URLS are not goint into already_searchet.txt

BASE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)))
print(BASE_DIR)


def pull_hrefs(browser, site):
    print("going to " + site)
    browser.get(site)
    time.sleep(5)
    href_text_list = []
    main_tag = browser.find_element_by_tag_name("main")
    a_tags = main_tag.find_elements_by_tag_name("a")
    print(len(a_tags))
    for i in range(0, len(a_tags)):
        try:
            href_text_list.append(a_tags[i].get_attribute("href"))
        except TypeError:
            pass
        except AttributeError:
            pass
    return href_text_list


def google_news(browser, search_term, site='https://news.google.com'):
    """Go's to any site, pulls out the href links"""
    uri = "/news/search/section/q/" + search_term + "/"
    href_text_list = pull_hrefs(browser, site + uri)
    try:
        with open('already_searched.txt', 'a+') as f:
            for link in range(0, len(href_text_list)):
                if href_text_list[link] not in f.read():
                    print("not in file, proceedings...")
                    *article, = try_this_link(href_text_list[link])
                    if article[0] == "article too small":
                        print("The Article was too small, the article writer is a lazy wanker!...")
                        continue
                    elif article[0] == "article too large":
                        print("Article must be under 10,000 words for the stingy cunts at Spinbot to approve it!...")
                        continue
                    else:
                        # send to spinbot
                        spun_article, original_article_title = spinbot_it(browser, article[0], article[1])
                        time.sleep(20)
                        # send to grammarly
                        grammarly_it(browser, spun_article, original_article_title)
                        print("Done! putting it in now")
                        f.write(href_text_list[link] + "\n")
    except NoSuchElementException:
        print("No such element on the page, what do you want to do if that occurs")


def heroku_it(url):
    time.sleep(5)
    a = Article(url, language='en')
    a.download()
    a.parse()
    return a.title, a.text, a.top_image


def try_this_link(url):
    *article, = heroku_it(url)
    if len(article[0] + article[1]) < 300:
        return "article too small"
    elif len(article[0] + article[1]) >= 10000:
        return "article too large"
    else:
        get_image(article[0], article[2])  # save image file to disk
        return article[0], article[1]


def get_image(article_title, article_top_image_url):
    filename = slugify(article_title) + ".jpg"  # get the slug version of the news page title
    response = requests.get(article_top_image_url)  # use the url of the image to get response data
    image_data = Image.open(BytesIO(response.content))  # save image data to a variable
    try:
        image_data.save(open(filename, "wb"))  # save the data as a file to the disk
    except IOError:
        Image.open(filename).convert("RGB").save(filename)


def spinbot_it(browser, original_article_title, original_article_body):
    time.sleep(1.2)
    browser.get("https://spinbot.com/")
    # Paste article title and body into Spinbot
    browser.find_element_by_xpath('//*[@id="form_textarea_text_before"]').send_keys(original_article_title + "\n" + original_article_body)
    # Paste in Negative keywords into spinbot
    ignore_words = "scam, smart, Bitclub, finance, bitcoin, cryptocurrency, crypto-currencies, crypto-currency, " \
                   "blockchain, ethereum, mining, difficulty, Ths, ghs, terahash, gigahash, block-chain, block, app, " \
                   "ledger, currency, us, usd, pound, gbp, uk, script, scrypt, server, servers, miners, miner"
    browser.find_element_by_xpath('//*[@id="spinForm"]/form/fieldset/table/tbody/tr/td[2]/ol/li[2]/table/tbody/tr/td['
                                  '2]/table/tbody/tr[2]/td/input').send_keys(ignore_words)
    # add in function if tick is not detected to wait and then restart after five minutes
    # Locate re-captcha box and clicks on the tick box
    while True:
        recaptcha_target = pyautogui.locateCenterOnScreen(os.path.join(BASE_DIR, "pics", "spinbot_recaptcha_box.png"))
        if recaptcha_target is None:
            time.sleep(1)
        else:
            pyautogui.moveTo(recaptcha_target, duration=3)
            pyautogui.moveRel(-126, 4, duration=1.1)
            pyautogui.click()
            time.sleep(5)
            break

    while True:
        green_tick = pyautogui.locateCenterOnScreen(os.path.join(BASE_DIR, "pics", "green-tick.png"))
        if green_tick is None:
            print("green tick not there!")
            time.sleep(5)
        else:
            # Locates the submit/go button and clicks on it
            spinbot_go = pyautogui.locateCenterOnScreen(os.path.join(BASE_DIR, "pics", "spinbot-go.png"))
            print("found green tick")
            pyautogui.moveTo(spinbot_go, duration=0.9)
            pyautogui.click()
            break
    time.sleep(20)
    # If the article has been successfully spun, copy the text
    spun_article = browser.find_element_by_css_selector('#form_textarea_text_after').text
    print(spun_article)
    return spun_article, original_article_title


def grammarly_login(browser, username, password):
    grammarlyLogin = browser.find_element_by_css_selector('._44aaab-userName > a:nth-child(1)')
    grammarlyLogin.click()
    time.sleep(12)
    input_boxes = browser.find_elements_by_tag_name("input")
    input_boxes[0].send_keys(username)
    input_boxes[1].send_keys(password)
    input_boxes[2].click()  # the submit button


def grammarly_it(browser, spun_article, original_article_title):
    browser.get("https://www.grammarly.com/")
    time.sleep(5)
    grammarly_login(browser, "kelseybitcoin@gmail.com", "L3ighann3")
    time.sleep(12)
    newGrammarlyFile = browser.find_element_by_css_selector("._4fdedb-add")
    newGrammarlyFile.click()
    time.sleep(5)
    textarea = browser.find_element_by_tag_name("textarea")
    textarea.click()
    textarea.send_keys(spun_article)
    time.sleep(10)
    # This part needs reworking so that it adapts to newly found grammar mistakes in the page.  It CAN'T be based on
    # the number of elements counted at the start.  The "cardBaseClassName" elements need to be recalculated evey
    # time they are clicked on and a new list needs to be generated every time so that the new set is taken into
    # account. This is important because "location_once_scrolled_into_view" may have changed on old grammar or may no
    # longer exist not to mention, new ones may also be added.

    # get all red errors on the page
    # get all delete parts from those
    # click on one delete part
    # empty all red errors and all delete parts
    # wait for page to calculate new red errors
    # rinse and repeat until delete parts equal 0 and then break


    delete_errors = []
    spans = browser.find_elements_by_tag_name("span")
    delete_critical_errors = browser.find_elements_by_class_name("#_929504-deleteReplacement")


    while len(delete_critical_errors) >= 1:
        delete_errors.append(delete_critical_errors)
        for i in range(0, len(delete_errors)):
            try:
                delete_critical_errors.pop().click()
            except:
                pass

    # delete_parts = []
    # while True:
    #     red_errors = browser.find_elements_by_class_name("cardBaseClassName")
    #     print(len(red_errors))
    #     for i in range(0, len(red_errors)):
    #         spans = red_errors[i].find_elements_by_tag_name("span")
    #         for span in spans:
    #             try:
    #                 delete_parts.append(span.find_element_by_class_name("_929504-deletePart"))
    #             except:
    #                 # not all spans will have a delete_part
    #                 pass
    #         delete_parts[span].location_once_scrolled_into_view()
    #         delete_parts[span].click()
    #         time.sleep(16)
    #     print(len(delete_parts))
    #     if len(delete_parts) == 0:
    #         break
    #     else:
    #         delete_parts = []
    # final_text = textarea.get_attribute("value")
    # with open(slugify(original_article_title) + ".txt", "w") as f:
    #     f.write(final_text)


firefox = webdriver.Firefox()
google_news(firefox, "bitcoin mining")

