import time
from settings import *
from slugify import slugify
from selenium.common.exceptions import (NoSuchElementException,
                                        ElementNotVisibleException,
                                        ElementNotInteractableException,
                                        StaleElementReferenceException,)
from chrome_enhanced import ChromeEnhanced


class Grammarly(ChromeEnhanced):
    def grammarly_login(self, username, password):
        self.get("https://www.grammarly.com/signin?page=free")
        input_boxes = self.tag_names("input")
        input_boxes[0].send_keys(username)
        input_boxes[1].send_keys(password)
        self.tag_name_with_click_then_delay('button', 6)

    def grammarly_new_file_click(self):
        time.sleep(2)
        self.css_click_with_delay("._4fdedb-add", 2)

    def load_text_from_file(self, filename=""):
        with open(filename, "r") as f:
            text = f.read()
        return text

    def switch_on_plagiarism(self):
        element1 = self.find_element_by_xpath('//*[@id="page"]/div/div/div/div[2]/div[1]/div[5]/div[2]')
        self.execute_script("arguments[0].setAttribute('class', '_747451-item _747451-genreItem _747451-hover')", element1)
        time.sleep(1)
        self.xpath_click_with_delay('//*[@id="page"]/div/div/div/div[2]/div[1]/div[5]/div[2]/div[2]/div/div[1]', 4)
        self.execute_script("arguments[0].setAttribute('class', '_747451-item _747451-genreItem')", element1)

    def grammarly_paste_text(self, spun_article=None):
        time.sleep(1)
        text_area = self.tag_name_with_click("textarea")
        if spun_article is None:
            spun_article = self.load_text_from_file("what-s-next-for-bitcoin-money-making-profitless-mining-beneficial.txt")
            # text_area.send_keys(spun_article)
            self.execute_script("arguments[0].value = arguments[1]", text_area, spun_article)
        else:
            # text_area.send_keys(spun_article)
            self.execute_script("arguments[0].value = arguments[1]", text_area, spun_article)
        return text_area

    def renew_cards(self):
        cards = self.find_elements_by_class_name("cardBaseClassName")
        return cards

    def make_report(self, order_number=""):
        # create report here
        # get the score
        time.sleep(10)
        element = self.find_elements_by_class_name('_ff9902-value')
        score = element.text
        if int(score) >= 95:
            time.sleep(2)
            # click score button
            element.click()
            time.sleep(2)
            plag_score = self.find_elements_by_class_name('_e118f2-row')[2].find_element_by_class_name(
                '_e118f2-value')
            if int(str(plag_score.text).strip('%')) <= 5:
                time.sleep(2)
                # click download button
                if os.path.isfile(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf")):
                    # delete it first if it exists in the downloads directory
                    os.remove(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf"))
                    time.sleep(2)
                    # then download it
                    self.xpath_click_with_delay('//*[@id="page"]/div/div/div/div[3]/div/div/div/div[2]/div[3]/div[1]', 20)
                    # after downloaded move it to the customers folder
                    os.renames(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf"),
                               os.path.join(os.getcwd(), "orders", order_number, order_number + "-report.pdf"))
                else:
                    # download it
                    self.xpath_click_with_delay('//*[@id="page"]/div/div/div/div[3]/div/div/div/div[2]/div[3]/div[1]', 20)
                    # after downloaded move it to the customers folder
                    os.renames(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf"),
                               os.path.join(os.getcwd(), "orders", order_number, order_number + "-report.pdf"))

    def grammarly_it(self, spun_article=None, filename="", extension="", order_number=""):
        self.grammarly_login(GRAMMARLY_USERNAME, GRAMMARLY_PASSWORD)
        self.grammarly_new_file_click()
        text_area = self.grammarly_paste_text(spun_article)
        self.switch_on_plagiarism()
        # record time here
        timeout = time.time() + 60 * 10  # set for 10 minutes at the moment
        while True:
            text_area.click()
            cards = self.renew_cards()
            print(len(cards))
            try:
                for i, card in enumerate(cards):
                    if card.text != "Undo":
                        # information part
                        print("Card number: " + str(i))
                        print("Total number of cards: " + str(len(cards)))
                        print("--------------------")
                        print("Card classes yaarrr!")
                        print("--------------------")
                        try:
                            print(card.get_attribute("class"))
                        except NoSuchElementException:
                            print("no more cards! yay!")
                        print("-------------------------------------------")
                        print("Card text, its not rocket science, darling!")
                        print("-------------------------------------------")
                        print(card.text)
                        try:
                            spans = card.find_elements_by_tag_name("span")
                        except NoSuchElementException:
                            pass
                        print("------------------------")
                        print("Card Span classes, yarr!")
                        print("------------------------")
                        for span in spans:
                            print(span.get_attribute("class"))
                        print("---------------------------------------------------------------------------------------")
                        # working part
                        location = card.location_once_scrolled_into_view
                        self.execute_script(
                            "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                        if card.text == "Squinting modifier" or "Wordiness" or "Dangling modifier" or "Incomplete comparison" or "Passive voice" or "Incorrect verb":
                            try:
                                ignore_button = card.find_element_by_class_name("_ed4374-btnIgnore")
                                try:
                                    ignore_button.click()
                                except ElementNotVisibleException:
                                    print("not visible")
                                    pass
                                break
                            except:
                                pass
                            continue
                        if card.text == "Possible Americanism" or "Unusual word pair":
                            card.click()
                            # time.sleep(1)
                            try:
                                span_element = card.find_element_by_class_name("_929504-listItemReplacement")
                                location = span_element.location_once_scrolled_into_view
                                print(location)
                                self.execute_script(
                                    "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                # time.sleep(1)
                                try:
                                    span_element.click()
                                except ElementNotVisibleException:
                                    print("not visible")
                                    pass
                                break
                            except:
                                pass
                        if len(str(card.text).split()) > 1:
                            if str(card.text).split()[0] + " " + str(card.text).split()[1] == "Overused word:" or "Weak adjective:" or "Repetitive word:":
                                card.click()
                                try:
                                    span_element = card.class_name("_929504-listItemReplacement")
                                    location = span_element.location_once_scrolled_into_view
                                    print(location)
                                    self.execute_script(
                                        "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                    try:
                                        span_element.click()
                                    except ElementNotVisibleException:
                                        print("not visible")
                                        pass
                                    break
                                except:
                                    pass
                        if len(str(card.text).split()) > 1:
                            if str(card.text).split()[0] + " " + str(card.text).split()[1] == "Unoriginal text:":
                                try:
                                    span_element = card.find_element_by_class_name("_ed4374-btnIgnore")
                                    try:
                                        span_element.click()
                                    except ElementNotVisibleException:
                                        print("not visible")
                                        pass
                                    break
                                except:
                                    pass
                        if card.text == "Possibly confused word":
                            card.click()
                            # time.sleep(1)
                            list_item_replacement = card.find_element_by_class_name("_929504-listItemReplacement")
                            location = list_item_replacement.location_once_scrolled_into_view
                            self.execute_script(
                                "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                            # time.sleep(1)
                            list_item_replacement.click()
                        for span in spans:
                            if span.get_attribute("class") == "_929504-insertPart":
                                try:
                                    print("deletePart")
                                    location = span.location_once_scrolled_into_view
                                    print(location)
                                    self.execute_script(
                                        "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                    # time.sleep(1)
                                    try:
                                        span.click()
                                    except ElementNotVisibleException:
                                        pass
                                    break
                                except ElementNotInteractableException:
                                    pass
                            elif span.get_attribute("class") == "_929504-insertReplacement":
                                try:
                                    print("insertReplacement")
                                    # span.location_once_scrolled_into_view
                                    location = span.location_once_scrolled_into_view
                                    print(location)
                                    self.execute_script(
                                        "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                    # time.sleep(1)
                                    try:
                                        span.click()
                                    except ElementNotVisibleException:
                                        pass
                                    break
                                except ElementNotInteractableException:
                                    pass
                            elif span.get_attribute("class") == "_929504-deletePunctuation":
                                try:
                                    print("deletePunctuation")
                                    # span.location_once_scrolled_into_view
                                    location = span.location_once_scrolled_into_view
                                    print(location)
                                    self.execute_script(
                                        "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                    # time.sleep(1)
                                    try:
                                        span.click()
                                    except ElementNotVisibleException:
                                        pass
                                    break
                                except ElementNotInteractableException:
                                    pass
                            elif span.get_attribute("class") == "_929504-deleteReplacement":
                                try:
                                    print("deleteReplacement")
                                    # span.location_once_scrolled_into_view
                                    location = span.location_once_scrolled_into_view
                                    print(location)
                                    self.execute_script(
                                        "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                    # time.sleep(1)
                                    try:
                                        span.click()
                                    except ElementNotVisibleException:
                                        pass
                                    break
                                except ElementNotInteractableException:
                                    pass
                            elif span.get_attribute("class") == "_929504-deletePart":
                                try:
                                    print("insertPart")
                                    # span.location_once_scrolled_into_view
                                    location = span.location_once_scrolled_into_view
                                    print(location)
                                    self.execute_script(
                                        "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                    # time.sleep(1)
                                    try:
                                        span.click()
                                    except ElementNotVisibleException:
                                        pass
                                    break
                                except ElementNotInteractableException:
                                    pass
                            elif card.text == "Incorrect quantifier":
                                try:
                                    location = card.location_once_scrolled_into_view
                                    print(location)
                                    self.execute_script(
                                        "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                                    # time.sleep(1)
                                    try:
                                        ignore_button = card.find_element_by_class_name("_ed4374-btnIgnore")
                                        ignore_button.click()
                                    except ElementNotVisibleException:
                                        pass
                                except ElementNotInteractableException:
                                    pass
                        print("Card number: " + str(i))
                        print("Total number of cards: " + str(len(cards)))
                        print("---------")
                        print("Card text")
                        print("---------")
                        print(card.text)
                        print("-------------------------------------")
                        print("Time left till timeout occurs: " + str(time.time() - timeout))
                        print("-------------------------------------")
                if len(cards) <= 1:
                    print("there is nothing left to process")
                    break
                if time.time() > timeout:
                    # time has reached 0 so give the results so far
                    # there was a cyclical loop so inform admin by sending an email
                    # TODO send email about grammarly timeout due to cyclical loop
                    text_area.click()
                    all_text = text_area.get_attribute("value")
                    self.make_report(order_number=order_number)
                    with open(os.path.join("orders", order_number, slugify(
                            filename.strip(".txt")) + '-' + order_number + "-corrected." + extension), "w") as f:
                        f.write(all_text)
                    return {"final_text": all_text}
            except StaleElementReferenceException:
                pass
        # this part happens if eveything went well
        text_area.click()
        all_text = text_area.get_attribute("value")
        self.make_report(order_number=order_number)
        # write the corrected version to disk
        with open(os.path.join("orders", order_number,
                               slugify(filename.strip(".txt")) + '-' + order_number + "-corrected." + extension), "w") as f:
            f.write(all_text)
        # remove the doc that we just created so that there is no trace we have been there!
        self.remove_the_latest_grammarly_document_created()
        return {"final_text": all_text}

    def remove_the_latest_grammarly_document_created(self):
        # assumes we are logged in
        trash_element = self.find_element_by_xpath(
            '//*[@id="page"]/div/div/div/div[2]/div[4]/div/div[3]/div[2]/div[3]/div/div/div')
        if self.current_url == 'https://app.grammarly.com/':
            trash_element.click()
        else:
            self.get('https://app.grammarly.com/')
            trash_element.click()
