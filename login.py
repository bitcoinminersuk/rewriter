import time
from selenium.webdriver import Chrome


def grammarly_login(browser, username, password):
    time.sleep(1)
    browser.get("https://www.grammarly.com/signin?page=free")
    input_boxes = browser.find_elements_by_tag_name("input")
    input_boxes[0].send_keys(username)
    input_boxes[1].send_keys(password)
    button = browser.find_element_by_xpath('//*[@id="page"]/div/div/div/form/div[1]/div[3]/button/span')
    button.click()  # the submit button
    time.sleep(6)


def fiverr_login(browser, username, password):
    browser.get("https://www.fiverr.com/")
    # click sign in link
    browser.find_element_by_css_selector("#main-wrapper-header>header>div.mp-box.mp-box-opaque>div>nav.main-nav.main-nav-guest>ul>li:nth-child(2)>a").click()
    # click login with google button
    browser.find_element_by_css_selector("#js-popup-user>div>div.popup-content-login>div.social-login-buttons.cf>a").click()
    time.sleep(4)
    # switch to the google window
    browser.switch_to_window("Fiverr_Google")
    # enter the username
    browser.find_element_by_css_selector("#identifierId").send_keys(username)
    # click the next button
    browser.find_element_by_css_selector("#identifierNext>content>span").click()
    time.sleep(3)
    # enter the password
    browser.find_element_by_css_selector("#password>div.aCsJod.oJeWuf>div>div.Xb9hP>input").send_keys(password)
    # click the next button
    browser.find_element_by_css_selector("#passwordNext>content>span").click()
    # switch back to the main window
    browser.switch_to_window("")
    time.sleep(3)
    # refresh the page
    browser.refresh()
    # we are now logged in phew!
    time.sleep(5)


def blog_login(browser, username, password):
    browser.get("https://bitcoinminersuk.com/accounts/login/")
    time.sleep(5)
    input_boxes = browser.find_elements_by_tag_name("input")
    input_boxes[2].send_keys(username)
    input_boxes[3].send_keys(password)
    input_boxes[4].click()  # the submit button


def choose(browser):
    answer = ""
    while answer == "":
        answer = input("choose your login? options(bitcoinminersuk.com(b), fiverr.com(f) or Grammarly.com(g): ")
        if answer.lower() == 'b':
            blog_login(browser, "bitcoinminersuk", "N3crophil3")
            break
        elif answer.lower() == 'f':
            fiverr_login(browser, "unison1978@gmail.com", "Kyriacou50")
            break
        elif answer.lower() == 'g':
            grammarly_login(browser, "chris99hansen@gmail.com", "christ9999")
            break
        answer = ""


chrome = Chrome()
choose(chrome)
