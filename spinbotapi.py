from System import *
from System.Diagnostics import *
from System.Net import *
from System.Net.Http import *
from System.Net.Http.Headers import *
from System.IO import *


textToSpin = "This is without a doubt the best article rewriting api in the world"
wordsToSkip = "rewrit,nonExistentWordPart"
percentToChange = "any"
spinCapWords = False
serviceUri = "https://api.spinbot.com"
authKey = "yourUniqueApiKey"
request = WebRequest.Create(serviceUri)
request.Headers.Add("x-auth-key:" + authKey)
request.Headers.Add("x-spin-cap-words:" + spinCapWords.ToString())
request.Headers.Add("x-words-to-skip:" + wordsToSkip)
request.Headers.Add("x-min-percent-change-per-sentence:" + percentToChange)
request.Method = "POST"
dataStream = request.GetRequestStream()
bytes = System.Text.Encoding.UTF8.GetBytes(textToSpin)
dataStream.Write(bytes, 0, bytes.Length)
dataStream.Close()
# Download data.
response = request.GetResponse()
# get response from the server
stream = response.GetResponseStream()
reader = StreamReader(stream)
# this is your altered text
spunText = reader.ReadToEnd()
# keep track of remaining spins so your app can alert when needs be
availableSpins = response.Headers["available-spins"]
# now do something with your results
Console.Out.WriteLine("\n\nSpun Text: " + spunText)
Console.Out.WriteLine("\n\nAvailable Spins: " + availableSpins)

