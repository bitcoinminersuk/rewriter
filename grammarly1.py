import os
import time
from selenium.common.exceptions import StaleElementReferenceException, ElementNotInteractableException
from slugify import slugify


try:
    BASE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)))
    print(BASE_DIR)
except NameError:
    BASE_DIR = os.path.join(os.path.join(os.path.abspath("rewriter2.py")))
    tmp = BASE_DIR.split("/")
    tmp.pop(-1)
    BASE_DIR = "/".join(tmp)
    print(BASE_DIR)


def load_text_from_file(filename=""):
    with open(filename, "r") as f:
        text = f.read()
    return text


def grammarly_login(browser, username, password):
    browser.get("https://www.grammarly.com/signin?page=free")
    input_boxes = browser.find_elements_by_tag_name("input")
    input_boxes[0].send_keys(username)
    input_boxes[1].send_keys(password)
    button = browser.find_element_by_css_selector("._233b6e-button")
    button.click()  # the submit button


def grammarly_new_file_click(browser):
    new_grammarly_file = browser.find_element_by_css_selector("._4fdedb-add")
    new_grammarly_file.click()
    time.sleep(5)


def grammarly_paste_text(browser, spun_article=None):
    textarea = browser.find_element_by_tag_name("textarea")
    textarea.click()
    if spun_article is None:
        spun_article = load_text_from_file("what-s-next-for-bitcoin-money-making-profitless-mining-beneficial.txt")
        textarea.send_keys(spun_article)
    else:
        textarea.send_keys(spun_article)


def renew_cards(browser):
    cards = browser.find_elements_by_class_name("cardBaseClassName")
    return cards


def grammarly_it(browser, spun_article=None, extension=""):
    grammarly_login(browser, "kelseybitcoin@gmail.com", "L3ighann3")
    time.sleep(15)
    grammarly_new_file_click(browser)
    grammarly_paste_text(browser, spun_article)
    time.sleep(10)
    while True:
        cards = renew_cards(browser)
        print(len(cards))
        try:
            for i, card in enumerate(cards):
                # information part
                print("Card number: " + str(i))
                print("Total number of cards: " + str(len(cards)))
                print("------------")
                print("Card classes")
                print("------------")
                print(card.get_attribute("class"))
                print("---------")
                print("Card text")
                print("---------")
                print(card.text)
                spans = card.find_elements_by_tag_name("span")
                print("-----------------")
                print("Card Span classes")
                print("-----------------")
                for span in spans:
                    print(span.get_attribute("class"))
                print("---------------------------------------------------------------------------------------")
                # working part
                card.location_once_scrolled_into_view
                if card.text == "Possibly confused word":
                    card.click()
                    time.sleep(5)
                    list_item_replacement = card.find_element_by_class_name("_929504-listItemReplacement")
                    list_item_replacement.location_once_scrolled_into_view
                    list_item_replacement.click()
                    print("Clicked the internal of a Possibly confused word")
                for span in spans:
                    if span.get_attribute("class") == "_929504-deletePart":
                        try:
                            span.location_once_scrolled_into_view
                            span.click()
                        except ElementNotInteractableException:
                            pass
                    elif span.get_attribute("class") == "_929504-insertReplacement":
                        try:
                            span.location_once_scrolled_into_view
                            span.click()
                        except ElementNotInteractableException:
                            pass
                    elif span.get_attribute("class") == "_929504-deletePunctuation":
                        try:
                            span.location_once_scrolled_into_view
                            span.click()
                        except ElementNotInteractableException:
                            pass
                    elif span.get_attribute("class") == "_929504-deleteReplacement":
                        try:
                            span.location_once_scrolled_into_view
                            span.click()
                        except ElementNotInteractableException:
                            pass
                    elif span.get_attribute("class") == "_929504-insertPart":
                        try:
                            span.location_once_scrolled_into_view
                            span.click()
                        except ElementNotInteractableException:
                            pass
                time.sleep(10)
        except StaleElementReferenceException:
            pass
        if len(cards) == 1:
            break
    # Copy final corrected text and save it to file
    textarea = browser.find_element_by_tag_name("textarea")
    all_text = textarea.get_attribute("value")
    print(all_text)
    newline_split = list(all_text.split("\n"))
    print(newline_split)
    spun_title = newline_split.pop(0)
    print(spun_title)
    newline_split.pop(0)
    print(newline_split)
    final_text = "\n".join(newline_split)
    print(final_text)
    # join list back together with newlines
    with open(slugify(spun_title) + ".txt", "w") as f:
        f.write(final_text)
    os.rename("image." + extension, slugify(spun_title) + "." + extension)
    return {"final_text": final_text, "spun_title": spun_title}


# def test_grammerly(browser):
#     grammarly_it(browser)
#
#
# firefox = webdriver.Firefox()
# test_grammerly(firefox)






