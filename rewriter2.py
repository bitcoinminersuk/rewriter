from io import BytesIO
from newspaper import Article
import os
import requests
import time
from PIL import Image
from selenium import webdriver
from selenium.common.exceptions import (NoSuchElementException,
                                        ElementNotVisibleException,
                                        ElementNotInteractableException,
                                        StaleElementReferenceException)
from slugify import slugify


try:
    BASE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)))
    print(BASE_DIR)
except NameError:
    BASE_DIR = os.path.abspath("rewriter2.py")
    tmp = BASE_DIR.split("/")
    tmp.pop(-1)
    BASE_DIR = "/".join(tmp)
    print(BASE_DIR)


def pull_hrefs(browser, site=""):
    print("going to " + site)
    browser.get(site)
    hrefs = []
    do_not_click_list = ["Sign in", "View full coverage", "RSS", "Other News Editions", "About Google News", "About Feeds", "Blog", "Help", "Privacy", "Terms of use", "Google Home", "Advertising Programs", "Business Solutions", "About Google"]
    elements = browser.find_elements_by_xpath("//*[@href]")
    for element in elements:
        try:
            if element.get_attribute("class") != "J3nBBd ME7ew" and element.tag_name != "script" and element.text != "":
                if element.text not in do_not_click_list:
                    hrefs.append(element.get_attribute("href"))
        except:
            pass
    return hrefs


def google_news(browser, search_term="", site='https://news.google.com'):
    """Go's to any site, pulls out the href links"""
    uri = "/news/search/section/q/" + search_term + "/"
    href_text_list = pull_hrefs(browser=browser, site=site + uri)
    try:
        with open('already_searched.txt', 'a+') as f:
            for link in range(0, len(href_text_list)):
                f.seek(0)
                if href_text_list[link] not in f.read():
                    print(href_text_list[link])
                    print("not in file, proceedings...")
                    article = try_this_link(url=href_text_list[link])
                    if article["text"] == "article too small":
                        print("The Article was too small, the article writer is a lazy wanker!...")
                        continue
                    elif article["text"] == "article too large":
                        print("Article must be under 10,000 words for the stingy cunts at Spinbot to approve it!...")
                        continue
                    else:
                        # send to spinbot  # TODO: check article size here also
                        spun_article = spinbot_it2(title=article["title"], text=article["text"])
                        if spun_article == "no spinbot credit":
                            # send email here
                            # TODO make a send email to warn about No spinbot credit
                            break
                        time.sleep(20)
                        # send to grammarly  # TODO: check article size here also
                        final_draft = grammarly_it(browser=browser, spun_article=spun_article["text"], extension=article["extension"])
                        # send to blog  # TODO: check article size here also
                        blog_submission(browser=browser, final_text=final_draft["final_text"], spun_title=final_draft["spun_title"], extension=article["extension"])
                        print("Done! putting it in now")
                        f.write(href_text_list[link] + "\n")  # TODO: text files need to be put into an articles folder
                        break
    except NoSuchElementException:
        print("No such element on the page, what do you want to do if that occurs")


def heroku_it(url=""):
    time.sleep(5)
    a = Article(url, language='en')
    a.download()
    a.parse()
    print("")
    print("--------------")
    print("heroku section")
    print("--------------")
    print(a.meta_data.get("news_keywords", "bitcoin steem ethereum mining btc"))  # news_keywords
    print("Meta Data")
    print(a.meta_data)
    print("Meta Keywords")
    print(a.meta_keywords)
    print("Meta Tags")
    print(a.tags)
    print("Meta Description")
    print(a.meta_description)  # A single string, e.g. Putin’s Internet ombudsman plans $100 million Initial Coin Offering (ICO) for Bitcoin mining farm in Russia. Per Dmitry Marinichev (Internet Ombudsman), the cry
    info_dict = {"title": a.title, "text": a.text, "top_image": a.top_image}
    return info_dict


def try_this_link(url=""):
    """ Takes an article URL, checks to see if its too small or too large and if the correct size 
    returns title, text and image file extention"""
    article = heroku_it(url=url)  # returns title, text, top_image, in that order as a dictionary
    if len(article["title"] + article["text"]) < 300:
        article["text"] = "article too small"
        return article
    elif len(article["title"] + article["text"]) >= 10000:
        article["text"] = "article too large"
        return article
    else:
        article["extension"] = get_image("image", article["top_image"])  # save image file to disk
        return article  # title, text and saved image file extension


def get_image(title="", top_image=""):  # this one uses response data
    response = requests.get(top_image)  # use the url of the image to get response data
    image_data = Image.open(BytesIO(response.content))# save image data to a variable
    extension = str(image_data.format).lower()
    filename = slugify(title) + "." + extension  # get the slug version of the news page title
    try:
        image_data.save(open(filename, "wb"))  # save the data as a file to the disk
        print("Saving image to file")
    except IOError:
        Image.open(filename).convert("RGB").save(filename)
        print("Saving image to file")
    return extension


def get_binary_image(title="", top_image=""):  # this one uses pillow
    image_data = Image.open(BytesIO(top_image))  # save image data to a variable
    extension = str(image_data.format).lower()
    filename = title + "." + extension  # get the slug version of the news page title
    try:
        image_data.save(open(filename, "wb"))  # save the data as a file to the disk
        print("Saving image to file")
    except IOError:
        Image.open(filename).convert("RGB").save(filename)
        print("Saving image to file")
    return extension


def spinbot_it2(title="", text=""):
    print("")
    print("-------------------")
    print("spinbot_it2 section")
    print("-------------------")

    ignore_words = "scam, smart, Bitclub, finance, bitcoin, cryptocurrency, crypto-currencies, crypto-currency, " \
                   "blockchain, ethereum, mining, difficulty, Ths, ghs, terahash, gigahash, block-chain, block, app, " \
                   "ledger, currency, us, usd, pound, gbp, uk, script, scrypt, server, servers, miners, miner"
    api_key = "69698b98c90c43cbb5497deee642294f"

    request_headers = {
        "x-auth-key": api_key,
        # "x-multi-creds": "true",
        # "x-spin-cap-words": "[true | false]",
        "x-words-to-skip": ignore_words
        # "x-min-percent-change-per-sentence": "[int between 1 - 100]",  # default is "any"
        # "x-action": "getavailablespins"
    }

    payload = (title + "\n" + text).encode(encoding='utf-8')

    r = requests.post('https://api.spinbot.com', data=payload, headers=request_headers)
    status_code = r.status_code
    print(r.encoding)

    if status_code == 200:
        print(str(status_code) + " " + r.reason)
        print("you see it work!")
        print(r.content.decode('utf-8'))
    else:
        print(str(status_code) + " " + r.reason)
        print("you see it not work!")
    spun_article = ""
    if int(r.headers["available-spins"]) > 1:
        article = r.content.decode('utf-8')
        parts = list(article.split("\n"))
        title = parts.pop(0)
        spun_article = {"title": title, "text": article}
    else:
        spun_article == "no spinbot credit"
    return spun_article


def load_text_from_file(filename=""):
    with open(filename, "r") as f:
        text = f.read()
    return text


def grammarly_login(browser, username, password):
    browser.get("https://www.grammarly.com/signin?page=free")
    input_boxes = browser.find_elements_by_tag_name("input")
    input_boxes[0].send_keys(username)
    input_boxes[1].send_keys(password)
    # // *[ @ id = "page"] / div / div / div / form / div[1] / div[3] / button / span
    button = browser.find_element_by_xpath('//*[@id="page"]/div/div/div/form/div[1]/div[3]/button/span')
    button.click()  # the submit button
    time.sleep(10)


def grammarly_new_file_click(browser):
    new_grammarly_file = browser.find_element_by_css_selector("._4fdedb-add")
    new_grammarly_file.click()
    time.sleep(5)


def grammarly_paste_text(browser, spun_article=None):
    textarea = browser.find_element_by_tag_name("textarea")
    textarea.click()
    if spun_article is None:
        spun_article = load_text_from_file("what-s-next-for-bitcoin-money-making-profitless-mining-beneficial.txt")
        textarea.send_keys(spun_article)
    else:
        textarea.send_keys(spun_article)


def renew_cards(browser):
    cards = browser.find_elements_by_class_name("cardBaseClassName")
    return cards


def grammarly_it(browser, spun_article=None, extension=""):
    grammarly_login(browser, "kelseybitcoin@gmail.com", "L3ighann3")
    time.sleep(15)
    grammarly_new_file_click(browser)
    grammarly_paste_text(browser, spun_article)
    time.sleep(10)
    # record time here
    timeout = time.time() + 60 * 10  # plus the 49 seconds that it takes to get through the next bit of code
    while True:
        cards = renew_cards(browser)
        print(len(cards))
        try:
            for i, card in enumerate(cards):
                # information part
                print("Card number: " + str(i))
                print("Total number of cards: " + str(len(cards)))
                print("--------------------")
                print("Card classes yaarrr!")
                print("--------------------")
                print(card.get_attribute("class"))
                print("-------------------------------------------")
                print("Card text, its not rocket science, darling!")
                print("-------------------------------------------")
                print(card.text)
                spans = card.find_elements_by_tag_name("span")
                print("------------------------")
                print("Card Span classes, yarr!")
                print("------------------------")
                for span in spans:
                    print(span.get_attribute("class"))
                print("---------------------------------------------------------------------------------------")
                # working part
                location = card.location_once_scrolled_into_view
                chrome.execute_script(
                    "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                time.sleep(5)
                if card.text == "Possibly confused word" and not card.text == "Undo":
                    card.click()
                    time.sleep(5)
                    list_item_replacement = card.find_element_by_class_name("_929504-listItemReplacement")
                    location = list_item_replacement.location_once_scrolled_into_view
                    chrome.execute_script(
                        "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                    time.sleep(1)
                    list_item_replacement.click()
                    print("Clicked the internal of a Possibly confused word")
                for span in spans:
                    if span.get_attribute("class") == "_929504-insertPart" and card.is_displayed() and not card.text == "Undo":
                        try:
                            print("deletePart")
                            location = span.location_once_scrolled_into_view
                            print(location)
                            chrome.execute_script(
                                "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                            time.sleep(4)
                            try:
                                span.click()
                            except ElementNotVisibleException:
                                pass
                            break
                        except ElementNotInteractableException:
                            pass
                    elif span.get_attribute("class") == "_929504-insertReplacement" and card.is_displayed() and not card.text == "Undo":
                        try:
                            print("insertReplacement")
                            # span.location_once_scrolled_into_view
                            location = span.location_once_scrolled_into_view
                            print(location)
                            chrome.execute_script(
                                "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                            time.sleep(4)
                            try:
                                span.click()
                            except ElementNotVisibleException:
                                pass
                            break
                        except ElementNotInteractableException:
                            pass
                    elif span.get_attribute("class") == "_929504-deletePunctuation" and card.is_displayed() and not card.text == "Undo":
                        try:
                            print("deletePunctuation")
                            # span.location_once_scrolled_into_view
                            location = span.location_once_scrolled_into_view
                            print(location)
                            chrome.execute_script(
                                "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                            time.sleep(4)
                            try:
                                span.click()
                            except ElementNotVisibleException:
                                pass
                            break
                        except ElementNotInteractableException:
                            pass
                    elif span.get_attribute("class") == "_929504-deleteReplacement" and card.is_displayed() and not card.text == "Undo":
                        try:
                            print("deleteReplacement")
                            # span.location_once_scrolled_into_view
                            location = span.location_once_scrolled_into_view
                            print(location)
                            chrome.execute_script(
                                "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                            time.sleep(4)
                            try:
                                span.click()
                            except ElementNotVisibleException:
                                pass
                            break
                        except ElementNotInteractableException:
                            pass
                    elif span.get_attribute("class") == "_929504-deletePart" and card.is_displayed() and not card.text == "Undo":
                        try:
                            print("insertPart")
                            # span.location_once_scrolled_into_view
                            location = span.location_once_scrolled_into_view
                            print(location)
                            chrome.execute_script(
                                "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                            time.sleep(4)
                            try:
                                span.click()
                            except ElementNotVisibleException:
                                pass
                            break
                        except ElementNotInteractableException:
                            pass
                    elif card.text == "Incorrect quantifier" and not card.text == "Undo":
                        try:
                            location = card.location_once_scrolled_into_view
                            print(location)
                            chrome.execute_script(
                                "window.scrollTo(" + str(0) + ", " + str(location["y"]) + ");")
                            time.sleep(4)
                            try:
                                ignore_button = card.find_element_by_class_name("_ed4374-btnIgnore")
                                ignore_button.click()
                            except ElementNotVisibleException:
                                pass
                        except ElementNotInteractableException:
                            pass
                print("Card number: " + str(i))
                print("Total number of cards: " + str(len(cards)))
                print("---------")
                print("Card text")
                print("---------")
                print(card.text)
                time.sleep(14)
        except StaleElementReferenceException:
            pass
        # check if cards equal 0 or time has reached 10 minutes and if so break
        if len(cards) <= 1 or time.time() > timeout:
            # TODO send email about grammarly timeout due to cyclical loop
            break
    # Copy final corrected text and save it to file
    textarea = browser.find_element_by_tag_name("textarea")
    all_text = textarea.get_attribute("value")
    newline_split = list(all_text.split("\n"))
    spun_title = newline_split.pop(0)
    newline_split.pop(0)
    final_text = "\n".join(newline_split)
    # join list back together with newlines
    with open(slugify(spun_title) + ".txt", "w") as f:
        f.write(final_text)
    spun_filename = slugify(spun_title) + "." + extension
    os.rename("image." + extension, spun_filename)  # TODO: images need to be put into an articles folder
    return {"final_text": final_text, "spun_title": spun_title}


# TODO: make a function to check for plagiarism
def plagiarism_test():
    pass


def blog_login(browser, username, password):
    browser.get("https://bitcoinminersuk.com/accounts/login/")
    time.sleep(5)
    input_boxes = browser.find_elements_by_tag_name("input")
    input_boxes[2].send_keys(username)
    input_boxes[3].send_keys(password)
    input_boxes[4].click()  # the submit button


def blog_submission(browser, final_text="", spun_title="", extension=""):
    blog_login(browser, "bitcoinminersuk", "N3crophil3")
    time.sleep(4)
    # goto Django admin
    browser.get("https://bitcoinminersuk.com/admin/posts/post/add/")
    # Paste Title
    blog_title = browser.find_element_by_css_selector("#id_title")
    blog_title.click()
    blog_title.send_keys(spun_title)
    # insert slug
    slug = browser.find_element_by_css_selector("#id_slug")
    slug.send_keys(slugify(spun_title))
    # Insert image location into clearableimageinput
    blog_image = browser.find_element_by_id("id_image")
    blog_image.clear()
    spun_filename = slugify(spun_title) + "." + extension
    blog_image.send_keys(os.path.join(os.getcwd(), spun_filename))
    # Paste content
    blog_content = browser.find_element_by_css_selector("#id_content")
    blog_content.click()
    blog_content.send_keys(final_text)
    # Click on the "Today" link, to put todays date in
    today = browser.find_element_by_css_selector("#post_form>div>fieldset>div.form-row.field-publish>div>p>span:nth-child(2)>a:nth-child(1)")
    today.click()
    # Click on the "Now" link, to put the time in as now
    now = browser.find_element_by_css_selector("#post_form>div>fieldset>div.form-row.field-publish>div>p>span:nth-child(5)>a:nth-child(1)")
    now.click()
    # Click on the save button
    save = browser.find_element_by_css_selector("#post_form>div>div>input.default")
    save.click()
    time.sleep(30)
    browser.get("https://bitcoinminersuk.com/blog/")


# TODO: send blog article to medium social blog

# TODO: send blog article to steamit

# TODO: submit our blog link to google

chrome = webdriver.Chrome()
google_news(chrome, search_term="bitcoin mining")


""" 
search_term = the main search term related to the website,
ignore_words = list of ignore words based on the subject search term,
spin_cap_words = true/false value based on whether if we want to spin capitalised words,
grammarly username and password,
blog site url, login (username and password),
blog site post/add/edit/create url after sucessful login,
login details for medium,
login details for steemit,
login details for google webmaster tools based on individual websites,


"""

# TODO: Think about clearing out and redoing dictionary add words based on each instance of new news search terms

# TODO: For the purpose of fiverr.com we need to make this script multi threaded in case we get lots of custom all at once
