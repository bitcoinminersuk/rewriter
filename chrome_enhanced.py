from selenium.webdriver import Chrome
import time


class ChromeEnhanced(Chrome):

    def __init__(self, *args, **kwargs):
        super(ChromeEnhanced, self).__init__(*args, **kwargs)

    def start_self(self):
        super()

    def goto_site(self, site):
        return self.get(site)

    def goto_site_with_delay(self, site, delay):
        result = self.goto_site(site)
        time.sleep(delay)
        return result

    def is_on_page(self, url):
        if self.current_url == url:
            return True
        else:
            return False

    ################################
    # Find Element By CSS Selector #
    ################################
    def css(self, selector):
        return self.find_element_by_css_selector(selector)

    def css_click(self, selector):
        return self.css(selector).click()

    def css_click_with_delay(self, selector, delay):
        result = self.css(selector).click()
        time.sleep(delay)
        return result

    def css_send_keys(self, selector, input_text):
        return self.css(selector).send_keys(input_text)

    #########################
    # Find Element By Xpath #
    #########################
    def xpath(self, xpath):
        return self.find_element_by_xpath(xpath)

    def xpath_with_delay(self, xpath, delay):
        result = self.xpath(xpath)
        time.sleep(delay)
        return result

    def xpath_click_with_delay(self, xpath, delay):
        result = self.xpath(xpath).click()
        time.sleep(delay)
        return result

    def xpath_text(self, xpath):
        return self.xpath(xpath).text

    ############################
    # Find Element By Tag Name #
    ############################
    def tag_name(self, tag_name):
        return self.find_element_by_tag_name(tag_name)

    def tag_names(self, tag_name):
        return self.find_elements_by_tag_name(tag_name)

    def tag_name_with_click(self, tag_name):
        return self.tag_name(tag_name).click()

    def tag_name_with_click_then_delay(self, tag_name, delay):
        result = self.tag_name(tag_name).click()
        time.sleep(delay)
        return result

    #############################
    # Find Element By Link Text #
    #############################
    def link_text(self, link_text):
        return self.find_element_by_link_text(link_text)

    def link_text_with_delay(self, link_text, delay):
        result = self.find_element_by_link_text(link_text)
        time.sleep(delay)
        return result

    def link_text_click(self, link_text):
        return self.link_text(link_text).click()

    def link_text_click_with_delay(self, link_text, delay):
        result = self.link_text(link_text).click()
        time.sleep(delay)
        return result
