# import os
#from rewriter.rewriter2 import blog_submission, get_image
import time
from selenium import webdriver


chrome = webdriver.Chrome()


def load_text_from_file(filename=""):
    with open(filename, "r") as f:
        text = f.read()
    return text


# steemit_spun_title = blog_submission(spun_title=True)
# steemit_spun_article = blog_submission(final_text=True)
# steemit_image = get_image(top_image=True)

# Login to Steemit
chrome.get("https://steemit.com/")
time.sleep(3)
chrome.find_element_by_css_selector("li.show-for-medium:nth-child(3)>a:nth-child(1)").click()
try:
    username = chrome.find_element_by_css_selector(".input-group-field")
    username.clear()
    username.send_keys("bitcoinminersuk")

    password = chrome.find_element_by_css_selector(".LoginForm>div:nth-child(1)>form:nth-child(2)>div:nth-child(2)>input:nth-child(1)")
    password.clear()
    password.send_keys("P5JikTedeUE7qbvqA46Fqr7QTV4bRFL7Xt2cy5hbv4QUtTwL5Qtr")
except:
    pass

try:
    login_button = chrome.find_element_by_css_selector("button.button:nth-child(2)")
    login_button.click()
except:
    pass

time.sleep(8)
# Submit an artcle
try:
    submit_article = chrome.find_element_by_css_selector("#content>div>header>div.Header__top.header>div>div.columns.shrink>ul>li.show-for-medium.submit-story>a")
    submit_article.click()
    time.sleep(4)
except:
    pass


# Article Title
article_title = chrome.find_element_by_css_selector(".ReplyEditor__title")
article_title.send_keys("The bitcoin mining and cyber security firm MGT Capital has brought $2.4 million up in new financing")

# Article Body
article_body = chrome.find_element_by_css_selector(".upload-enabled")
file_up = load_text_from_file("mgt-capital-raises-2-4-million-to-grow-bitcoin-mining-operation.txt")
article_body.send_keys(file_up)

# Artilce Hashtags
article_tags = chrome.find_element_by_css_selector("div.vframe__section--shrink:nth-child(4)>span:nth-child(1)>span:nth-child(1)>input:nth-child(1)")
article_tags.send_keys("bitcoin mining ethereum steem btc")
time.sleep(15)

# Submit
final_submission = chrome.find_element_by_css_selector("button.button:nth-child(1)")
try:
    final_submission.click()
except:
    pass




# steemit URL
# https://steemit.com/

# login link (css selector)
# li.show-for-medium:nth-child(3)>a:nth-child(1)

# login username (css selector)
# .input-group-field

# login password (css selector)
# .LoginForm>div:nth-child(1)>form:nth-child(2)>div:nth-child(2)>input:nth-child(1)

# login button (css selector)
# button.button:nth-child(2)

# submit a article (css selector)
# .sub-menu>li:nth-child(2)>a:nth-child(1)

# article title (css selector)
# .ReplyEditor__title

# article body (css selector)
# .upload-enabled

# tag (css selector)
# div.vframe__section--shrink:nth-child(4)>span:nth-child(1)>span:nth-child(1)>input:nth-child(1)

