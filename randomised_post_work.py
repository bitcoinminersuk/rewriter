import os
import time
import requests
import re
import pyautogui
import dropbox
from selenium.webdriver.remote.webelement import WebElement
from random import randrange
from ast import literal_eval
from selenium.webdriver import Chrome
from selenium.common.exceptions import (NoSuchElementException,
                                        # ElementNotVisibleException,
                                        # ElementNotInteractableException,
                                        StaleElementReferenceException,
                                        WebDriverException,)

from settings import (SPINBOT_API_KEY, GRAMMARLY_USERNAME, GRAMMARLY_PASSWORD, FIVERR_USERNAME, FIVERR_PASSWORD,
                      FIVERR_DROPBOX_ACCESS_TOKEN, DOWNLOADS_DIRECTORY, BASE_DIR, ORDERS_PATH, COMPLETED_PATH,
                      ACCEPTED_FILE_EXTENSIONS)


class CheckState:
    """
    Set self.folder to the folder of your choice on the inheriting class e.g.
    self.folder = "james_is_gay_all_the_time" and by that I mean happy!... when he has a finger up his bum
    """

    def __init__(self, folder=None):
        self.folder = folder

    @staticmethod
    def get_orders():
        """
        Gets a list of folders based on your current working directory and a path or folder name
        :return: returns a list of folder names
        """
        return os.listdir(ORDERS_PATH)

    @staticmethod
    def get_folder_date(order):
        """
        Gets a single date for a folder when it was last modified.
        :return: last modification date of when Chris last visited his friend(singular) on grindr
        """
        return str(os.stat(os.path.join(ORDERS_PATH, order, os.sep)).st_mtime)

    def get_folder_dates(self):
        """
        Gets a list of dates for each folder when it was last modified.
        :return: last modification date of when Chris last visited his friends of grindr
        """
        dates = []
        for order in self.get_orders():
            dates.append(os.stat(os.path.join(BASE_DIR, self.folder, order)).st_mtime)
        return dates

    def save_folder_state_on_disk(self):
        """
        Takes a folder, and creates a file in the format of <folder_name>-state.txt and
        places it in the Current Working Directory
        :return: writes file to disk
        """
        orders = self.get_orders()
        dates = self.get_folder_dates()
        with open(self.folder + '_state.txt', 'w') as f:
            f.write(str(list(zip(orders, dates))))

    def check_folder_has_changed(self):
        """
        Checks whether or not the state of any directory has changed, based on the
        file created using the self.save_folder_state_on_disk function which must be run
        to this function to work
        :return: Boolean returns True if state of the folder is different from the
        state recorded in the file.
        """
        directory_state_now = os.listdir(os.path.join(BASE_DIR, self.folder))
        with open(self.folder + '_state.txt', 'r') as f:
            recorded_state = f.read()
            if directory_state_now != recorded_state:
                return True
            else:
                return False


class StatusFile(CheckState):
    @staticmethod
    def status_file_exists():
        return os.path.isfile(os.path.join(BASE_DIR, "order_status.txt"))

    def status_file_order_exists(self, order):
        return self.status_file_check(order, "id")

    def status_file_initialise_order(self, order, original=False):
        if original:
            return str({"id": order,
                        "time_created": self.get_folder_date(order),
                        "random_time": None,
                        "original": original,
                        "dropbox_share_link": None,
                        "original_filename": "",
                        "Spinbot": False,
                        "Grammarly": False,
                        "uploaded": False,
                        "Fiverr": False,  # this is set when the order has been sent and is complete
                        }) + "\n"
        else:
            print("Can't initialise an order, file has not been downloaded")

    def status_file_add_random_times(self):
        folders = os.listdir(ORDERS_PATH)  # get all order folders
        # for every order random_time key give it a random time
        if not folders == []:
            for folder in folders:
                if self.status_file_check(folder, "random_time") is None:
                    folder_time = os.stat(os.path.join(ORDERS_PATH, folder)).st_mtime
                    random_time = str(folder_time + randrange((60 * 2) * 60, (60 * 5) * 60))
                    self.status_file_update(folder, "random_time", value=random_time)

    def status_file_check(self, order, stage):
            try:
                with open("order_status.txt", "r") as f:
                    for line_number, line in enumerate(f.readlines()):
                        if line.find(order) != -1:
                            line = literal_eval(line)
                            print("got " + stage + ": " + str(line.get(stage)))
                            if line.get(stage):
                                return str(line.get(stage))
                            else:
                                return False
            except FileNotFoundError:
                with open("order_status.txt", "w"):
                    pass
                return self.status_file_check(order, stage)

    def status_file_update(self, order, stage, value=None):
        with open("order_status.txt", "r+") as f:
            for line_number, line in enumerate(f.readlines()):
                if line.find(order) != -1:
                    line = literal_eval(line)
                    line[stage] = value
                    line = str(line)
                    f.seek(line_number)
                    f.write(line + "\n")

    def status_file_get_original_order_filename(self, order):
        return self.status_file_check(order, "original_filename")

    def status_file_add_new_order(self, order, original=False):
        with open("order_status.txt", "a+") as f:
            f.write(self.status_file_initialise_order(order, original=original))

    def status_file_get_order_line_number(self, order):
        try:
            with open("order_status.txt", "r") as f:
                for line_number, line in enumerate(f.readlines()):
                    if line.find(order) != -1:
                        return line_number
        except FileNotFoundError:
            with open("order_status.txt", "w"):
                pass
            return self.status_file_get_order_line_number(order)

    def status_file_get_all_lines(self):
        """
        Retrieves all information about orders and sets
        :return:
        """
        try:
            with open("order_status.txt", "r") as f:
                return f.readlines()
        except FileNotFoundError:
            with open("order_status.txt", "w"):
                pass
            return self.status_file_get_all_lines()


class FileStuff:
    @staticmethod
    def load_text_from_file(filename=""):
        with open(filename, "r") as f:
            text = f.read()
        return text

    @staticmethod
    def separate_filename_from_extension(filename):
        filename, extension = os.path.splitext(filename)
        return filename, extension

    def move_file(self, order, filename, from_path, to_path, postfix=None):
        filename, extension = self.separate_filename_from_extension(filename)
        from_file = os.path.join(from_path, "{}{}".format(filename, extension))
        to_file = os.path.join(to_path, order, "{}_{}_{}{}".format(order, filename, postfix, extension))
        os.renames(from_file, to_file)
        new_filename = str(to_file.split(os.path.sep)[-1])
        return new_filename, to_file

    @staticmethod
    def move_folder_to_completed(order):
        os.renames(os.path.join(ORDERS_PATH, order), os.path.join(COMPLETED_PATH, order))

    @staticmethod
    def create_completed_order_file_name(order, filename):
        return "{0}{1}{2}{1}{3}.txt".format(order, "_", filename.strip(".txt"), "corrected")

    @staticmethod
    def create_original_order_file_name(order, filename):
        return "{0}{1}{2}{1}{3}.txt".format(order, "_", filename.strip(".txt"), "original")

    def create_original_order_full_path(self, order, filename):
        return os.path.join(ORDERS_PATH, order, self.create_original_order_file_name(order, filename))

    def create_completed_order_full_path(self, order, filename):
        return os.path.join(ORDERS_PATH, order, self.create_completed_order_file_name(order, filename))

    @staticmethod
    def check_order_file_exists_on_disk(order, filename):
        if os.path.isfile(os.path.join(ORDERS_PATH, order, filename)):
            return True
        else:
            return False

    @staticmethod
    def create_folder(order, folder_category):
        if not os.path.exists(os.path.join(BASE_DIR, folder_category, order)):
            os.makedirs(os.path.join(BASE_DIR, folder_category, order))

    @staticmethod
    def create_text_file(text, filename, is_order=None):
        order = filename.split('_')[0]
        if is_order:
            filename = os.path.join(ORDERS_PATH, order, filename)
            with open(filename, "w") as f:
                f.write(text)
        else:
            with open(filename, "w") as f:
                f.write(text)

    @staticmethod
    def extract_text_and_title(content):
        if content:
            split_content = str(content).split("\n")
            print(split_content)
            title = split_content.pop(0)
            if split_content == "":
                split_content.pop(0)
            text = ''.join(split_content)
            return title, text


class Spinbot(object):

    @staticmethod
    def replace_common_unicode_with_ascii(content):
        if content:
            content = re.sub(u"\u2014", "--", content)
            content = re.sub(u"\u201c", '"', content)
            content = re.sub(u"\u201d", '"', content)
            content = re.sub(u"\u2019", "'", content)
        return content

    @staticmethod
    def remove_newlines_from_keywords(ignore_words):
        ignore_words = ignore_words.replace("\r\n", "")
        ignore_words = ignore_words.replace("\n", "")
        return ignore_words

    def spinbot_it(self, title="", text="", ignore_words=""):
        print("")
        print("-------------------")
        print("| spinbot section |")
        print("-------------------")
        print("")
        # initial values so if something goes wrong it returns something useful
        spun_article = {"title": "Spinbot service is down", "text": "Spinbot service is down"}
        # dummy = {
        #     "x-auth-key": SPINBOT_API_KEY,
        #     "x-multi-creds": "true",
        #     "x-spin-cap-words": "[true | false]",
        #     "x-words-to-skip": ignore_words
        #     "x-min-percent-change-per-sentence": "[int between 1 - 100]",  # default is "any"
        #     "x-action": "getavailablespins"
        # }
        # this part fixes issues with ignore_words making spinbot crash
        ignore_words = self.remove_newlines_from_keywords(ignore_words)

        request_headers = {
            "x-auth-key": SPINBOT_API_KEY,
            "x-words-to-skip": ignore_words
        }

        request_available_spins = {
            "x-auth-key": SPINBOT_API_KEY,
            "x-action": "getavailablespins"
        }

        text = self.replace_common_unicode_with_ascii(title + "\n" + text)
        payload = text.encode(encoding='utf-8').decode()
        print("")
        print("--------------------------")
        print("| Text before conversion |")
        print("--------------------------")
        print("")
        print(payload)
        print("")
        # Gets the available spin credits and prints it
        r = requests.post('https://api.spinbot.com', data="", headers=request_available_spins)
        print("Available Spins Left: " + str(r.headers["available-spins"]))
        print(r)
        if r.status_code == 200 and int(r.headers["available-spins"]) > 0:
            print(str(r.status_code) + " " + r.reason)
            print("you see it work!")
            if int(r.headers["available-spins"]) > 0:
                # the next line spins it
                r = requests.post('https://api.spinbot.com', data=payload, headers=request_headers)
                print("spinning...")
                print("Available Spins Left: " + str(r.headers["available-spins"]))
                time.sleep(10)
                article = r.content.decode('utf-8')
                print("")
                print("-------------------------")
                print("| Text After conversion |")
                print("-------------------------")
                print("")
                parts = article.split("\n")
                title = parts.pop(0)
                if parts[0] == "":
                    parts.pop(0)
                text = ''.join(parts)
                spun_article = {"title": title, "text": text}
            else:
                print("You have no available credits go to: https://spinbot.com/Manage to top up")
                spun_article = {"title": "no spinbot credit", "text": "no spinbot credit"}
        else:
            print(str(r.status_code) + " " + r.reason)
            print("you see it not work!")
        return spun_article


class ChromeEnhanced(Chrome):

    def __init__(self):
        super(ChromeEnhanced, self).__init__()

    def goto_site(self, site):
        self.get(site)

    def goto_site_with_delay(self, site, delay):
        self.goto_site(site)
        time.sleep(delay)

    def is_on_page(self, url):
        if self.current_url == url:
            return True
        else:
            return False

    @staticmethod
    def get_parent(element):
        return element.find_element_by_xpath('..')

    #########################
    # Find Element By Class #
    #########################

    def class_name(self, class_name):
        return self.find_element_by_class_name(class_name)

    def class_name_with_click(self, class_name):
        return self.class_name(class_name).click()

    def class_name_click_with_delay(self, class_name, delay):
        result = self.class_name(class_name).click()
        time.sleep(delay)
        return result

    def class_name_send_keys(self, class_name, input_text):
        return self.class_name(class_name).send_keys(input_text)

    def class_names(self, class_name):
        return self.find_elements_by_class_name(class_name)

    ################################
    # Find Element By CSS Selector #
    ################################
    def css(self, selector):
        return self.find_element_by_css_selector(selector)

    def css_click(self, selector):
        self.css(selector).click()

    def css_click_with_delay(self, selector, delay):
        self.css(selector).click()
        time.sleep(delay)

    def css_send_keys(self, selector, input_text):
        self.css(selector).send_keys(input_text)

    #########################
    # Find Element By Xpath #
    #########################
    def xpath(self, xpath):
        return self.find_element_by_xpath(xpath)

    def xpath_with_delay(self, xpath, delay):
        result = self.xpath(xpath)
        time.sleep(delay)
        return result

    def xpath_click_with_delay(self, xpath, delay):
        result = self.xpath(xpath).click()
        time.sleep(delay)
        return result

    def xpath_text(self, xpath):
        return self.xpath(xpath).text

    ############################
    # Find Element By Tag Name #
    ############################
    def tag_name(self, tag_name):
        return self.find_element_by_tag_name(tag_name)

    def tag_names(self, tag_name):
        return self.find_elements_by_tag_name(tag_name)

    def tag_name_with_click(self, tag_name):
        return self.tag_name(tag_name).click()

    def tag_name_with_click_then_delay(self, tag_name, delay):
        result = self.tag_name(tag_name).click()
        time.sleep(delay)
        return result

    #############################
    # Find Element By Link Text #
    #############################
    def link_text(self, link_text):
        return self.find_element_by_link_text(link_text)

    def link_text_with_delay(self, link_text, delay):
        result = self.find_element_by_link_text(link_text)
        time.sleep(delay)
        return result

    def link_text_click(self, link_text):
        return self.link_text(link_text).click()

    def link_text_click_with_delay(self, link_text, delay):
        result = self.link_text(link_text).click()
        time.sleep(delay)
        return result

    ##############################################
    # Functions that talk directly to Javascript #
    ##############################################
    def scroll_to(self, x=None, y=None):
        if x and y is not None:
            self.execute_script(
                'window.scrollTo(' + str(x) + ', ' + str(y) + ');')
        elif x is not None:
            self.execute_script(
                'window.scrollTo(' + str(x) + ', ' + '"0");')
        elif y is not None:
            self.execute_script(
                'window.scrollTo("0", ' + str(y) + ');')
        else:
            # does nothing if no coordinates are passed into the top
            pass


class Grammarly(ChromeEnhanced, FileStuff):

    def __init__(self):
        super(Grammarly, self).__init__()

    def grammarly_login(self, username, password):
        self.get("https://www.grammarly.com/signin?page=free")
        input_boxes = self.tag_names("input")
        input_boxes[0].send_keys(username)
        input_boxes[1].send_keys(password)
        self.tag_name_with_click_then_delay('button', 6)

    def grammarly_new_file_click(self):
        time.sleep(2)
        self.css_click_with_delay("._4fdedb-add", 10)

    def grammarly_switch_on_plagiarism(self):
        elem = self.xpath('//*[@id="page"]/div/div/div/div[2]/div[1]/div[5]/div[2]')
        self.execute_script("arguments[0].setAttribute('class', '_747451-item _747451-genreItem _747451-hover')", elem)
        time.sleep(1)
        self.xpath_click_with_delay('//*[@id="page"]/div/div/div/div[2]/div[1]/div[5]/div[2]/div[2]/div/div[1]', 4)
        self.execute_script("arguments[0].setAttribute('class', '_747451-item _747451-genreItem')", elem)

    def grammarly_paste_text(self, text=None):
        time.sleep(1)
        text_area = self.tag_name("textarea")
        if text is None:
            text = self.load_text_from_file("what-s-next-for-bitcoin-money-making-profitless-mining-beneficial.txt")
            self.execute_script("arguments[0].value = arguments[1]", text_area, text)
        else:
            self.execute_script("arguments[0].value = arguments[1]", text_area, text)
        return text_area

    def grammarly_make_report(self, order=""):
        time.sleep(10)  # wait here is only so we can see the results, it's not necessary
        element = self.class_name('_ff9902-value')
        score = element.text
        if int(score) >= 95:
            time.sleep(2)
            # click score button
            element.click()
            time.sleep(2)
            plag_score = self.class_names('_e118f2-value')[2]
            if int(str(plag_score.text).strip('%')) <= 5:
                time.sleep(2)
                xpath = '//*[@id="page"]/div/div/div/div[3]/div/div/div/div[2]/div[3]/div[1]'  # download report xpath
                if os.path.isfile(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf")):  # delete it if it exists
                    os.remove(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf"))
                    time.sleep(2)
                    self.xpath_click_with_delay(xpath, 20)
                    os.renames(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf"),
                               os.path.join(ORDERS_PATH, order, order + "_report.pdf"))
                else:
                    self.xpath_click_with_delay(xpath, 20)
                    os.renames(os.path.join(DOWNLOADS_DIRECTORY, "report.pdf"),
                               os.path.join(ORDERS_PATH, order, order + "_report.pdf"))

    def grammarly_click_inside_card(self, card):
        card.find_element_by_class_name("_ed4374-header").click()
        time.sleep(1)
        span_element = card.find_element_by_class_name("_929504-listItemReplacement")
        location = span_element.location_once_scrolled_into_view
        self.scroll_to(x=0, y=location["y"])
        time.sleep(1)
        span_element.click()
        print("clicked on " + span_element.text)

    def grammarly_it(self, spun_article=None, filename="", order_number=""):
        # login
        self.grammarly_login(GRAMMARLY_USERNAME, GRAMMARLY_PASSWORD)
        # click new
        self.grammarly_new_file_click()
        # paste text
        text_area = self.grammarly_paste_text(spun_article)
        # switch on plagiarism
        self.grammarly_switch_on_plagiarism()
        ignore_cards = ["Squinting modifier", "Wordiness", "Dangling modifier", "Incomplete comparison",
                        "Passive voice", "Incorrect verb", "Incorrect quantifier"]
        click_in_card = ["Possible Americanism", "Unusual word pair"]
        click_in_card_ending_in_colon = ["Overused word", "Weak adjective", "Repetitive word",
                                         "Possibly miswritten word"]
        timeout = time.time() + 60 * 10  # set for 10 minutes at the moment
        while True:
            text_area.click()
            cards = self.find_elements_by_class_name('cardBaseClassName')
            print("**** renewed cards *****")
            for card in cards:
                try:
                    if str(card.text) != "Undo":
                        print("")
                        print(card.text)
                        location = card.location_once_scrolled_into_view
                        self.scroll_to(y=location["y"])
                        spans = card.find_elements_by_tag_name('span')
                        for span in spans:
                            if span.get_attribute("class") == "_929504-insertPart":
                                print("insertPart")
                                location = span.location_once_scrolled_into_view
                                self.scroll_to(y=location["y"])
                                time.sleep(1)
                                span.click()
                            elif span.get_attribute("class") == "_929504-insertReplacement":
                                print("insertReplacement")
                                location = span.location_once_scrolled_into_view
                                self.scroll_to(y=location["y"])
                                time.sleep(1)
                                span.click()
                            elif span.get_attribute("class") == "_929504-deletePunctuation":
                                print("deletePunctuation")
                                location = span.location_once_scrolled_into_view
                                self.scroll_to(y=location["y"])
                                time.sleep(1)
                                span.click()
                            elif span.get_attribute("class") == "_929504-deleteReplacement":
                                print("deleteReplacement")
                                location = span.location_once_scrolled_into_view
                                self.scroll_to(y=location["y"])
                                time.sleep(1)
                                span.click()
                            elif span.get_attribute("class") == "_929504-deletePart":
                                print("deletePart")
                                location = span.location_once_scrolled_into_view
                                self.scroll_to(y=location["y"])
                                time.sleep(1)
                                span.click()
                        if str(card.text) in click_in_card:
                            self.grammarly_click_inside_card(card)
                        elif str(card.text).split(':')[0] in click_in_card_ending_in_colon:
                            self.grammarly_click_inside_card(card)
                        elif str(card.text) == "Possibly confused word":
                            self.grammarly_click_inside_card(card)
                        elif str(card.text) in ignore_cards:
                            card.find_element_by_class_name("_ed4374-btnIgnore").click()
                            print("clicked ignore on " + card.text)
                        elif str(card.text).split(':')[0] == "Unoriginal text":
                            card.find_element_by_class_name("_ed4374-btnIgnore").click()
                            print("clicked ignore on " + card.text)
                        # card.find_element_by_class_name("_ed4374-header").click()
                        time.sleep(2)
                except StaleElementReferenceException:
                    print("stale")
                    break
                except NoSuchElementException:
                    pass
                except WebDriverException:
                    pass
            text_area.click()
            time.sleep(10)
            if len(cards) <= 1:
                print("there is nothing left to process")
                break
            if time.time() > timeout:
                # time has reached 0 so give the results so far
                # there was a cyclical loop so inform admin by sending an email
                # TODO send email about grammarly timeout due to cyclical loop
                text_area.click()
                all_text = text_area.get_attribute("value")
                self.grammarly_make_report(order=order_number)
                return {"final_text": all_text}
        all_text = text_area.get_attribute("value")
        self.grammarly_make_report(order=order_number)
        # remove the doc that we just created so that there is no trace we have been there!
        self.grammarly_remove_latest_document()
        return {"final_text": all_text}

    def grammarly_save_to_disk(self, order, filename, content):
        built_corrected_path = self.create_completed_order_full_path(order, filename)
        os.makedirs(os.path.dirname(built_corrected_path), exist_ok=True)
        with open(built_corrected_path, "w") as f:
            f.write(content)

    def grammarly_remove_latest_document(self):
        if self.current_url == 'https://app.grammarly.com/':
            self.execute_script("document.getElementsByClassName('_4fdedb-delete')[0].click()")
            #
            # location = pyautogui.locateCenterOnScreen(os.path.join(PICS_PATH, "today.png"))
            # print("found today text without redirecting")
            # print("pyautogui today image location: " + str(location))
            # pyautogui.moveTo(location, duration=2)
            # pyautogui.moveRel(73, 98)
            # while True:
            #     time.sleep(1)
            #     print(pyautogui.position())
            # pyautogui.click()
            # time.sleep(5)
            # trash_element = self.find_element_by_xpath(
            #     '//*[@id="page"]/div/div/div/div[2]/div[4]/div/div[3]/div[2]/div[3]/div/div/div')
            # trash_element.click()
        else:
            self.goto_site_with_delay('https://app.grammarly.com/', 8)
            self.execute_script("document.getElementsByClassName('_4fdedb-delete')[0].click()")
            # self.class_name_with_click('_4fdedb-delete')
            #
            # print("found today text after redirect")
            # location = pyautogui.locateCenterOnScreen(os.path.join(PICS_PATH, "today.png"))
            # print("pyautogui today image location: " + str(location))
            # pyautogui.moveTo(location, duration=2)
            # pyautogui.moveRel(73, 98)

            # while True:
            #     time.sleep(1)
            #     print(pyautogui.position())
            # pyautogui.click()
            # time.sleep(5)
            # trash_element = self.find_element_by_xpath(
            #     '//*[@id="page"]/div/div/div/div[2]/div[4]/div/div[3]/div[2]/div[3]/div/div/div')
            # trash_element.click()


class FiverrControl(Grammarly, Spinbot, StatusFile):

    def __init__(self, username, password, order_directory="orders"):
        super(FiverrControl, self).__init__()
        self.username = username
        self.password = password
        self.folder = order_directory

    def fiverr_check_if_logged_in(self):
        """
        Looks to see if the Dashboard element exists on the fiver website and if it does that means we are logged in,
        crude but effective! (for now anyway)
        :return:
        True if logged in, False otherwise
        """
        try:
            self.css("#main-wrapper-header>header>div>div>nav.main-nav.main-nav-user>ul>li.menu-icn.icn-todo.hint--bottom.js-mark>a")
            return True
        except NoSuchElementException:
            return False

    def fiverr_check_if_logged_in_and_if_not_then_login(self):
        if not self.fiverr_check_if_logged_in():
            self.fiverr_login_with_google()

    def fiverr_login_with_google(self):
        if not self.fiverr_check_if_logged_in():
            self.goto_site_with_delay("https://www.fiverr.com/", 10)
            # click sign in link
            self.find_element_by_link_text("Sign In").click()
            time.sleep(4)
            # click login with google button
            self.find_element_by_class_name('btn-social-connect').click()
            time.sleep(4)
            self.find_element_by_id("js-popup-user").click()
            time.sleep(8)
            # switch to the google window
            self.switch_to.window("Fiverr_Google")
            # enter the username
            self.css_send_keys("#identifierId", self.username)
            # click the next button
            self.css_click_with_delay("#identifierNext>content>span", 3)
            # enter the password
            self.css_send_keys("#password>div.aCsJod.oJeWuf>div>div.Xb9hP>input", self.password)
            # click the next button
            self.css_click("#passwordNext>content>span")
            # switch back to the main window
            self.switch_to.window("")
            time.sleep(3)
            # refresh the page
            self.refresh()
            # we are now logged in phew!
            time.sleep(5)

    def fiverr_download_original_file(self, order):  # TODO: needs a lot of work
        if self.is_on_page("https://www.fiverr.com/users/christophers150/manage_orders/" + order):
            filename = str(self.class_name("filename").text)
            file_link = str(self.class_name('file-meta').get_attribute('href'))
            extension = os.path.splitext(filename)[-1]
            built_full_path = self.create_original_order_full_path(order, filename)
            built_filename = self.create_original_order_file_name(order, filename)
            accepted_file_extensions = ACCEPTED_FILE_EXTENSIONS
            if str(extension).strip(".") in accepted_file_extensions:  # extension exists
                if self.status_file_exists():
                    if self.status_file_order_exists(order):
                        if self.status_file_check(order, "original_filename") == filename and self.check_order_file_exists_on_disk(order, filename):
                            new_filename = built_filename
                            content = self.load_text_from_file(built_full_path)
                            return content, new_filename, filename
                        else:
                            self.goto_site_with_delay(file_link, 10)  # downloads the file
                            new_filename, new_from_path = self.move_file(order, filename, DOWNLOADS_DIRECTORY,
                                                                         ORDERS_PATH,
                                                                         postfix="original")
                            content = self.load_text_from_file(new_from_path)
                            self.status_file_update(order, "original_filename", value=filename)
                            return content, new_filename, filename
                    else:
                        self.status_file_add_new_order(order, original=True)
                        return self.fiverr_download_original_file(order)
                else:
                    self.status_file_add_new_order(order, original=True)
                    return self.fiverr_download_original_file(order)
            else:
                # send a message to the customer to tell them to use the right extension
                pass
        else:
            self.fiverr_order_detail_page(order)
            return self.fiverr_download_original_file(order)

    def fiverr_get_negative_keywords(self, order):
        if self.is_on_page("https://www.fiverr.com/users/christophers150/manage_orders/" + order):
            return str(self.class_names("answer-as-text")[2].text)
        else:
            self.fiverr_order_detail_page(order)
            return self.fiverr_get_negative_keywords(order)

    def fiverr_unhide_requirements(self):
        header = str(self.xpath('//*[@id="main-wrapper"]/div[4]/div/section/div[1]/article/section[2]').get_attribute("class"))
        collapsed = header.split()
        if 'collapsed' in collapsed:
            time.sleep(2)
            print('saw collapsed')
            self.xpath('//*[@id="main-wrapper"]/div[4]/div/section/div[1]/article/section[2]/article[1]/header/p/a/span[2]').click()
            time.sleep(2)

    def fiverr_get_gig_requirements(self, order):
        content, new_filename, filename = self.fiverr_download_original_file(order)  # sets "original_filename" in status_order.txt
        negative_keywords = self.fiverr_get_negative_keywords(order)
        return new_filename, filename, content, negative_keywords, order

    def fiverr_goto_order_list_page(self):
        self.goto_site_with_delay("https://www.fiverr.com/users/christophers150/manage_orders/", 5)

    def fiverr_get_order_links(self):
        table_body = self.tag_name('tbody')
        try:
            a_tags = table_body.find_elements_by_tag_name('a')[1]
        except IndexError:
            return []
        if isinstance(a_tags, WebElement):
            return [a_tags]
        elif isinstance(a_tags, list):
            return a_tags

    def fiverr_order_list_top_link_click(self):
        try:
            self.css_click_with_delay("#manage_sales>div>div.db-new-main-table>table>tbody>tr>td.ellipsis-wrap>div"
                                      ":nth-child(2)>div>a", 6)
        except NoSuchElementException:
            print("there are no gigs!")
            # wait 60 seconds
            for i in range(60, -1, -1):
                time.sleep(1)
                print("time in seconds left till next retry: " + str(i))
            self.fiverr_order_list_top_link_click()

    def fiverr_get_order_number(self):
        return str(self.xpath_text('//*[@id="main-wrapper"]/div[4]/div/section/div[1]/article/header/h1')).split()[1].strip('#')

    def fiverr_order_detail_page(self, order):
        self.get("https://www.fiverr.com/users/christophers150/manage_orders/" + order)

    def fiverr_process_order(self, order):
        new_filename, original_filename, content, negative_keywords, order = self.fiverr_get_gig_requirements(order)
        spun_article = {}
        if self.status_file_order_exists(order):
            if not self.status_file_check(order, "Spinbot"):
                # do spin
                if content:
                    title, text = self.extract_text_and_title(content)
                    spun_article = self.spinbot_it(title=title, text=text, ignore_words=negative_keywords)
                    # the spinbot service was down
                    if spun_article["title"] == "Spinbot service is down":
                        # wait an hour before running this again
                        time.sleep(60*60)  # Its not feasible to do it here but we can have a "delay_by" flag in the order_status.txt file
                        spun_article = self.spinbot_it(title=title, text=text, ignore_words=negative_keywords)
                    # the spinbot credit was depleted
                    if spun_article["title"] == "no spinbot credit":
                        # send email to admin
                        time.sleep(60*15)  # same as comment that is lined up with this one above
                        # check spinbot api for credit before trying again
                        spun_article = self.spinbot_it(title=title, text=text, ignore_words=negative_keywords)
                    # when we complete the spinbot part its not a good idea to mark "Spinbot" in the status file to
                    # true until the corrected version is saved to disk this will happen after the Grammarly phase is
                    # complete
            if not self.status_file_check(order, "Grammarly"):
                # do Grammarly
                spun_article = spun_article["title"] + "\n\n" + spun_article["text"]
                final_text = self.grammarly_it(spun_article=spun_article, filename=new_filename, order_number=order)
                print(final_text["final_text"])
                # save state to order_status.txt here
                print(order)
                self.status_file_update(order, "Spinbot", value=True)
                self.status_file_update(order, "Grammarly", value=True)
                self.grammarly_save_to_disk(order, original_filename, final_text["final_text"])
            if not self.status_file_check(order, "Fiverr"):
                self.fiverr_send_order(order)
        else:
            self.status_file_add_new_order(order, original=True)
            self.fiverr_process_order(order)

    def dropbox_upload_files(self, order_number):
        """
        Takes in an order number and gets all files that are contained in that order number folder,
        sends them to dropbox in and puts them in a folder named "Order - <order_number>"
        :param order_number:
        Takes a String that is used to read and create folders and files of the same name,
        if the folder and/or files with the corresponding order number embeded into them do not exist
        then this wont work.
        :return:
        Returns a list of dictionaries for each file it uploaded / or not as the case may be!
        """
        order_folder_for_dropbox = "Order - " + order_number
        files = os.listdir(os.path.join(ORDERS_PATH, order_number))  # get all files in directory
        response = []
        if not self.status_file_check(order_number, "uploaded"):
            for i, file in enumerate(files):
                with open(os.path.join(ORDERS_PATH, order_number, file), "rb") as f:
                    try:
                        response = dbx.files_upload(f.read(), os.path.join(os.sep, order_folder_for_dropbox, file))
                        print("uploading file " + str(i+1) + " of " + str(len(files)))
                        time.sleep(20)
                    except:
                        print("Tried to upload " + file + " but it was already there! skippings...")
            self.status_file_update(order_number, "uploaded", value=True)
        return response

    def dropbox_share_link(self, order_number):
        share_link = dbx.sharing_create_shared_link_with_settings(os.path.join(os.sep, "Order - " + order_number)).url
        self.status_file_update(order_number, "dropbox_share_link", value=share_link)
        return share_link

    def fiverr_send_order(self, order):
        time_now = time.time()
        self.status_file_add_random_times()
        if self.status_file_order_exists(order) == order:
            if self.status_file_check(order, "random_time") is not None:
                send_time = self.status_file_check(order, "random_time")
                print(str(time.time()) + " time now")
                print(str(send_time) + " time order is supposed to happen")
                if time_now > send_time:
                    #  uploading files
                    self.dropbox_upload_files(order)
                    share_link = self.dropbox_share_link(order)
                    self.goto_site_with_delay("https://www.fiverr.com/users/christophers150/manage_orders/" + order, 4)
                    self.xpath_click_with_delay('//*[@id="action-bar"]/div/div/div[1]/a', 6)
                    thank_you_message = """As promised, I have supplied you with a revised version of your article within the 24-hour window.

                                        Included is a Grammarly report to certify the material has been optimised to be pass error and plagiarism thresholds.
                
                                        Here is the link to your revised files: {}
                
                                        I would appreciate a good review and hope to do further business with you.
                
                                        Kindest regards
                
                
                                        Chris""".format(share_link)
                    self.find_element_by_xpath('//*[@id="msg_body"]').send_keys(thank_you_message)
                    self.xpath_click_with_delay('//*[@id="new_delivery_v2"]/footer/div/input', 10)
                    # the fucking wankers have popped up a box asking if we really want to complete the order without
                    self.switch_to.alert.accept()
                    # location = pyautogui.locateCenterOnScreen(os.path.join(BASE_DIR, "pics", "gay_ok_button.png"))
                    # print("pyautogui ok button location: " + str(location))
                    # pyautogui.click(location)
                    time.sleep(5)
                    # move the files to the completed folder
                    self.status_file_update(order, "Fiverr", value=True)
                    self.move_folder_to_completed(order)
            else:
                self.status_file_add_random_times()
                self.fiverr_send_order(order)

    # def check_fiverr_for_order(self):
    #     if not self.fiverr_check_if_logged_in():
    #         self.fiverr_login_with_google()
    #     self.fiverr_goto_order_list_page()
    #     self.fiverr_order_list_top_link_click()
    #     order = self.fiverr_get_gig_requirements()
    #     self.fiverr_process_order()
    #
    # def test(self):
    #     # self.FiverrControl(FIVERR_USERNAME, FIVERR_PASSWORD)
    #     self.fiverr_login_with_google()
    #     self.fiverr_process_order()


if __name__ == "__main__":
    dbx = dropbox.Dropbox(FIVERR_DROPBOX_ACCESS_TOKEN)
    test = FiverrControl(FIVERR_USERNAME, FIVERR_PASSWORD)
    while True:
        test.fiverr_check_if_logged_in_and_if_not_then_login()
        test.fiverr_goto_order_list_page()
        links = test.fiverr_get_order_links()
        for link in links:
            time.sleep(4)
            print(link.text)
            link.click()
            time.sleep(6)
            orderings = test.fiverr_get_order_number()
            print(orderings)
            test.fiverr_unhide_requirements()
            time.sleep(4)
            if not test.status_file_check(orderings, "Fiverr"):
                if not test.status_file_order_exists(orderings) and not test.status_file_check(orderings, "Grammarly"):
                    test.fiverr_process_order(orderings)
                if test.status_file_check(orderings, "Grammarly"):
                    test.fiverr_send_order(orderings)
            # countdown till next round
            seconds = 10  # set the timer here, this is a good candidate for the settings file
            for i in range(seconds, -1, -1):
                time.sleep(1)
                print("time in seconds left till next retry: " + str(i), "\b", end="\r")
            print("                                                                                      ", end="\r")

    # while True:
    #     # check status_file... this checks certain stages of the process
    #     for line in test.status_file_get_all_lines():
    #         parts = line.split(', ')
    #         print(parts)

        # if orders exist
        # process orders
        # check to see if the folder order number matches the order in the top link and see if it has a random time...
        # if all that is true
        # if test.status_file_check(obligation, "id") and test.status_file_check(obligation, "Grammarly"):
        #     test.fiverr_send_order(obligation)
        # elif test.status_file_check(obligation, "id"):
        #     test.fiverr_process_order()

    # setting up the instances
    # orders = OrdersCheckState()
    # completed = CompletedCheckState()

    # using the instances
    # completed.save_folder_state_on_disk()

    # for order in completed.get_orders():
    #     completed.get_folder_date(order)
    #     print("the date for " + order + " is " + completed.get_folder_date(order))

    # check to see if the filename is txt
    # if not send them a patronising message like the last one
    # download the file in the aside
    # move the original file into a local "orders/<order_number>/<order_number>_<filename>_original.txt" directory
    # do the spinbot(set Spinbot True), grammarly(set Spinbot True) steps
    # download report.pdf and move it into a local "orders/<order_number>/<order_number>_<filename>.pdf" directory
    # move the corrected file into a local "orders/<order_number>/<order_number>_<filename>_corrected.txt" directory
    # upload files to Dropbox and send message including link
    # could give them 5 stars auto-magically here!
