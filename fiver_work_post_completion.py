# check order folder for changes
# if any new folders come in add to a running dictionary with a random
# time between 2 and 5 hours later than the date of the folder
# check all folders dates and compare with times recorded against them
# if the times match or are grater than the current time then process that order
import os
from random import randrange
import time
from selenium import webdriver
import pyautogui
import dropbox
import shutil

# ---------------------------- #
# | commonly shared settings | #
# ---------------------------- #

BASE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)))
DOWNLOADS_DIRECTORY = os.path.join(os.sep, "home", "chris", "Downloads")
SPINBOT_API_KEY = "69698b98c90c43cbb5497deee642294f"
GRAMMARLY_USERNAME = "chris99hansen@gmail.com"
GRAMMARLY_PASSWORD = "christ9999"

# ------------------- #
# | fiverr settings | #
# ------------------- #

FIVERR_USERNAME = "unison1978@gmail.com"
FIVERR_PASSWORD = "Kyriacou50"
# fiverr_service app
FIVERR_DROPBOX_ACCESS_TOKEN = 'WBbA7tcNZ5AAAAAAAAAALLa17fOrs-Sze_mat1WFav6I3q4ojPkHW83dyjlVrpid'

# --------------------------------- #
# | dummy settings template header| #
# --------------------------------- #


dbx = dropbox.Dropbox(FIVERR_DROPBOX_ACCESS_TOKEN)
# current_directory_state = []


# def check_folder_for_changes():
#     directory_state_now = os.listdir(os.path.join(os.getcwd(), "orders"))
#     if directory_state_now == current_directory_state:
#         print("directory Structure is still the same")
#         return False
#     else:
#         print("directory Structure has changed")
#         return True

# if file is not empty but order folder is empty then delete content from file
def add_randomised_time_to_orders():
    orders = os.listdir(os.path.join(os.getcwd(), "orders"))  # get all order folders
    completed = os.listdir(os.path.join(os.getcwd(), "completed"))  # get all completed order folders
    print("orders: " + str(orders))
    print("completed: " + str(completed))
    we_can_clear_the_file_safely = False
    # create or append to file
    with open("order_list_times.txt", "a+") as f:
        lines = f.readlines()
        print(len(lines))
        if orders is [] and lines is []:
            we_can_clear_the_file_safely = True
        for x, order in enumerate(orders):
            if order is [line.split(",")[0] for line in lines] and order not in completed:
                print(order + " is already in the process pipeline")
            elif order in completed and order not in orders:
                print(order + " has already been processed, removing...")
                shutil.rmtree(os.path.join(os.getcwd(), "orders", order))
                lines.pop(x)
                f.seek(0)
                f.write(lines)
            elif order in orders:
                folder_date = os.stat(os.path.join(os.getcwd(), "orders", order)).st_mtime
                random_time = folder_date + randrange((60 * 2) * 60, (60 * 4) * 60)
                f.write(order + "," + str(random_time) + "\n")
                print("added time to " + order + " the date set is " + str(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(random_time))))
    if we_can_clear_the_file_safely:
        open("order_list_times.txt", 'w').close()
    with open("order_list_times.txt", "r") as f:
        lines = f.readlines()
        orders = [line.split(",")[0] for line in lines]
        send_time = [float(line.split(",")[1]) for line in lines]
    orders_with_times = zip(orders, send_time)
    return orders_with_times


def order_times_info():
    with open("order_list_times.txt", "r") as f:
        for item in f.readlines():
            if item is not []:
                order = item.split(",")[0]
                date = item.split(",")[1]
                print("Time " + order + " will be sent is: " + str(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(date)))))


def fiverr_login(browser, username, password):
    # try to find the dashboard icon to check if we are already logged in
    try:
        browser.find_element_by_css_selector(
            "#main-wrapper-header>header>div>div>nav.main-nav.main-nav-user>ul>li.menu-icn.icn-todo.hint--bottom.js-mark>a")
        already_logged_in = True
    except:  # NoSuchElementException:
        # if exception is rased then it means that the icon was not found which means we are logged out
        already_logged_in = False
    if not already_logged_in:
        browser.get("https://www.fiverr.com/")
        time.sleep(4)
        # click sign in link
        browser.find_element_by_css_selector("#main-wrapper-header>header>div.mp-box.mp-box-opaque>div>nav.main-nav.main-nav-guest>ul>li:nth-child(2)>a").click()
        # click login with google button
        browser.find_element_by_css_selector("#js-popup-user>div>div.popup-content-login>div.social-login-buttons.cf>a").click()
        time.sleep(4)
        # switch to the google window
        browser.switch_to_window("Fiverr_Google")
        # enter the username
        browser.find_element_by_css_selector("#identifierId").send_keys(username)
        # click the next button
        browser.find_element_by_css_selector("#identifierNext>content>span").click()
        time.sleep(3)
        # enter the password
        browser.find_element_by_css_selector("#password>div.aCsJod.oJeWuf>div>div.Xb9hP>input").send_keys(password)
        # click the next button
        browser.find_element_by_css_selector("#passwordNext>content>span").click()
        # switch back to the main window
        browser.switch_to_window("")
        time.sleep(3)
        # refresh the page
        browser.refresh()
        # we are now logged in phew!
        time.sleep(5)


def move_file_to_completed(folder, file):
    shutil.move(os.path.join(os.getcwd(), "orders", folder, file),
                os.path.join(os.getcwd(), "completed", folder, file))


def move_folder_to_completed(folder):
    try:
        shutil.move(os.path.join(os.getcwd(), "orders", folder),
                    os.path.join(os.getcwd(), "completed", folder))
    except FileNotFoundError:
        print("File not found, nothing to move!")


def upload_files_to_dropbox(order_number):
    order_folder_for_dropbox = "Order - " + order_number
    files = os.listdir(os.path.join(os.getcwd(), "orders", order_number))  # get all files in directory
    for file in files:
        with open(os.path.join(os.getcwd(), "orders", order_number, file), "rb") as f:
            try:
                dbx.files_upload(f.read(), os.path.join(os.sep, order_folder_for_dropbox, file))
                # once uploaded, move them to the completed folder
                move_file_to_completed(order_number, file)
                time.sleep(20)
            except:
                print("Tried to upload the files but they were already there! skipping...")
                # if they are already there we still need to move them to the completed folder
                move_file_to_completed(order_number, file)


def get_dropbox_share_link(order_number):
    return dbx.sharing_create_shared_link_with_settings(os.path.join(os.sep, "Order - " + order_number)).url


def process_order_2(browser, orders_with_times):
    fiverr_login(browser, FIVERR_USERNAME, FIVERR_PASSWORD)
    print("order with times " + str(list(orders_with_times)))
    for order in orders_with_times:
        time_now = time.time()
        print("time now is : " + str(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time_now))))
        print(order[0] + " to be sent at: " + str(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(order[1]))))
        if time_now > order[1]:
            browser.get("https://www.fiverr.com/users/christophers150/manage_orders/" + order[0])
            work_delivered_text = str(browser.find_element_by_xpath('//*[@id="action-bar"]/div/div/div[3]/p').text)
            if not work_delivered_text == 'WORK\nDELIVERED':
                # click on the "deliver now" button
                browser.find_element_by_xpath('//*[@id="action-bar"]/div/div/div[1]/a').click()
                # wait for the form to appear
                time.sleep(2)
                # upload the files
                upload_files_to_dropbox(order[0])
                time.sleep(30)
                # get a link for the customer to access their files on Dropbox with
                share_link = get_dropbox_share_link(order[0])
                thank_you_message = """As promised, I have supplied you with a revised version of your article within the 24-hour window.
    
                                        Included is a Grammarly report to certify the material has been optimised to be pass error and plagiarism thresholds.
    
                                        Here is the link to your revised files: {}
    
                                        I would appreciate a good review and hope to do further business with you.
    
                                        Kindest regards
    
    
                                        Chris""".format(share_link)
                browser.find_element_by_xpath('//*[@id="msg_body"]').send_keys(thank_you_message)
                browser.find_element_by_xpath('//*[@id="new_delivery_v2"]/footer/div/input').click()
                time.sleep(10)
                # the fucking wankers have poped up a box asking if we really want to complete the order without uploading files
                location = pyautogui.locateCenterOnScreen(os.path.join(BASE_DIR, "pics", "gay_ok_button.png"))
                pyautogui.click(location)
                move_folder_to_completed(order[0])
                # remove from order_list_times.txt
                time.sleep(5)
                shutil.rmtree(os.path.join(os.getcwd(), "orders", order[0]))
            else:
                try:
                    # remove entry from file
                    with open("order_list_times.txt", 'w') as f:
                        lines = f.readlines()
                        print("order in else try " + order)
                    shutil.rmtree(os.path.join(os.getcwd(), "orders", order[0]))
                except FileNotFoundError:
                    # remove entry from file
                    with open("order_list_times.txt", 'w') as f:
                        lines = f.readlines()
                        print("order in else except " + order)
                    print("Already moved")
                move_folder_to_completed(order[0])


def process_order(browser, orders):
    fiverr_login(browser, FIVERR_USERNAME, FIVERR_PASSWORD)
    for order in orders:
        browser.get("https://www.fiverr.com/users/christophers150/manage_orders/" + order)
        work_delivered_button = str(browser.find_element_by_xpath('//*[@id="action-bar"]/div/div/div[3]/p').text)
        with open("order_list_times.txt", "r") as f:
            f.seek(0)
            lines = f.readlines()
            order = [line.split(",")[0] for line in lines]
            send_time = [float(line.split(",")[1]) for line in lines]
            time_now = time.time()
            print("time now is : " + str(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time_now))))
            print(order + " to be sent at: " + str(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(send_time))))
        if time_now > send_time:
            # try:
            time.sleep(4)
        if not work_delivered_button == 'WORK\nDELIVERED':
            # click on the "deliver now" button
            browser.find_element_by_xpath('//*[@id="action-bar"]/div/div/div[1]/a').click()
            # wait for the form to appear
            time.sleep(6)
            # upload the files
            upload_files_to_dropbox(order)
            time.sleep(30)
            # get a link for the customer to access their files on Dropbox with
            share_link = get_dropbox_share_link(order)
            thank_you_message = """As promised, I have supplied you with a revised version of your article within the 24-hour window.
    
                                    Included is a Grammarly report to certify the material has been optimised to be pass error and plagiarism thresholds.
    
                                    Here is the link to your revised files: {}
    
                                    I would appreciate a good review and hope to do further business with you.
    
                                    Kindest regards
    
    
                                    Chris""".format(share_link)
            browser.find_element_by_xpath('//*[@id="msg_body"]').send_keys(thank_you_message)
            browser.find_element_by_xpath('//*[@id="new_delivery_v2"]/footer/div/input').click()
            time.sleep(10)
            # the fucking wankers have poped up a box asking if we really want to complete the order without uploading files
            location = pyautogui.locateCenterOnScreen(os.path.join(BASE_DIR, "pics", "gay_ok_button.png"))
            pyautogui.click(location)
            move_folder_to_completed(order)
            # remove from order_list_times.txt
            time.sleep(5)
        if work_delivered_button == 'WORK\nDELIVERED':
            print("detected that this job has already been done (Work Delivered")
            # move folder to completed
            move_folder_to_completed(order)
            # remove from order_list_times.txt
            # except:
            #     print(browser.current_url)
            #     if browser.current_url == "https://www.fiverr.com/users/christophers150/manage_orders":
            #         move_folder_to_completed(order)
            #     else:
            #         process_order(browser=browser)


def main():
    chrome = webdriver.Chrome()
    while True:
        orders_with_times = add_randomised_time_to_orders()
        print(orders_with_times)
        process_order_2(chrome, orders_with_times)
        order_times_info()
        for i in range(10, -1, -1):
            time.sleep(1)
            print("time in seconds left till next retry: " + str(i), "\b", end="\r")
        print("                                                                                      ", end="\r")


if __name__ == "__main__":
    main()
